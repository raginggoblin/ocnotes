# Ocnotes

OcNotes is the desktop companion of NextCloud notes [(https://github.com/nextcloud/notes)](https://github.com/nextcloud/notes). It is a small program that keeps track of your notes. The program normally sits hidden in your system tray. By clicking on it, your notes are painted on your desktop as colored sticky notes. It can be used on any desktop with a Java Runtime Environment, version 8 or higher.

Though the client is supposed to run in conjunction with an NextCloud installation, the client is perfectly usable as a standalone program. You can configure the program to synchronize with your NextCloud installation. See the NextCloud website for further information about NextCloud. OcNotes is written in Java and released under the Gnu Public License.

<p align="center"><img src="https://gitlab.com/raginggoblin/ocnotes/raw/master/etc/screenshots/Systray.png" /></p>
<p align="center"><img src="https://gitlab.com/raginggoblin/ocnotes/raw/master/etc/screenshots/OcNotesGui.png" /></p>
<p align="center"><img src="https://gitlab.com/raginggoblin/ocnotes/raw/master/etc/screenshots/ColoredNotes.png" /></p>
<p align="center"><img src="https://gitlab.com/raginggoblin/ocnotes/raw/master/etc/screenshots/NarrowLayout.png" /></p>


# Features
- Keep track of your notes through colored sticky notes on your desktop.
- Synchronize the sticky notes with NextCloud notes.
- Coloring, spacing and size of sticky notes is configurable.
- When no system tray is available, the program is usable through the gui.

# How to run
1. OcNotes requires java 8 or higher (http://www.oracle.com/technetwork/java/javase/downloads/index.html).
2. Download [OcNotes.zip](https://gitlab.com/raginggoblin/ocnotes/uploads/663ad7e5c810f66b6d5fb0a2e47ccfeb/OcNotes.zip) and extract the contents to a convenient location.
2. On Linux run 'java -jar OcNotes.jar'. On Windows dubble click OcNotes.jar or run 'java -jar OcNotes.jar'.
3. OcNotes requires a fully loaded system tray. I noticed that this takes some time and sometimes OcNotes is loaded before the system tray is up. I therefore use a small loading script at startup:

<pre class="prettyprint">
#!/bin/bash
sleep 10
cd /home/raginggoblin/OcNotes/
java -jar OcNotes.jar &
</pre>

# FAQ
#####1. Why do the fonts on Windows look crappy?
Appearantly the default fonts are terrible on a Windows box. You can configure the fonts to make them look better.


Visit https://raginggoblin.wordpress.com/ocnotes for more information.
