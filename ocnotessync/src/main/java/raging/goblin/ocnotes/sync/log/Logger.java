/*
 * Copyright 2015, RagingGoblin <http://raginggoblin.wordpress.com>
 *
 * This file is part of OcNotes.
 *
 *  OcNotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OcNotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OcNotes.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.ocnotes.sync.log;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public interface Logger {

    DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;

    default void log(LogLevel level, String message, Exception e) {
        System.out.println(String.format("%s %s: %s", formatter.format(LocalDateTime.now()), level.name(), message));
        e.printStackTrace();
    }

    default void log(LogLevel level, String message) {
        System.out.println(String.format("%s %s: %s", formatter.format(LocalDateTime.now()), level.name(), message));
    }

}
