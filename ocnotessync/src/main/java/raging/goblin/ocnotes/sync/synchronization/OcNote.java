/*
 * Copyright 2015, RagingGoblin <http://raginggoblin.wordpress.com>
 *
 * This file is part of OcNotes.
 *
 *  OcNotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OcNotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OcNotes.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.ocnotes.sync.synchronization;

import java.time.Instant;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonIgnoreProperties(ignoreUnknown = true, value = {"title", "value"})
public class OcNote {

    private int id;
    private long modified;
    private String category;
    private String content;
    private boolean favorite;

    public OcNote(int id) {
        this(id, Instant.now().getEpochSecond(), "New note");
    }

    public OcNote(final int id, final long modified, final String content) {
        this.id = id;
        this.modified = modified;
        this.content = content;
    }

    public OcNote(OcNote other) {
        this.update(other);
    }

    public String getTitle() {
        return Optional.ofNullable(content)
                .map(content -> content.split(System.lineSeparator())[0])
                .orElse("");
    }

    public String getValue() {
        return Optional.ofNullable(content)
                .map(content -> Arrays.asList(content.split(System.lineSeparator())).stream()
                        .skip(1L)
                        .collect(Collectors.joining(System.lineSeparator())))
                .orElse("");
    }

    public void update(OcNote other) {
        id = other.id;
        modified = other.modified;
        category = other.category;
        content = other.content;
        favorite = other.favorite;
    }
}
