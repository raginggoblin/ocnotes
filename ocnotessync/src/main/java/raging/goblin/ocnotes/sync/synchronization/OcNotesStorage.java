/*
 * Copyright 2015, RagingGoblin <http://raginggoblin.wordpress.com>
 *
 * This file is part of OcNotes.
 *
 *  OcNotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OcNotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OcNotes.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.ocnotes.sync.synchronization;

import java.util.List;
import java.util.Optional;

/**
 * OcNotesStorage storage for {@link OcNote}s this can either be a remote storage through a webservice or a local
 * storage.
 */
public interface OcNotesStorage {

    List<OcNote> updateOcNotes(List<OcNote> notes, String username, String password);

    List<OcNote> addOcNotes(List<OcNote> notes, String username, String password);

    void removeOcNotes(List<OcNote> notes, String username, String password);

    List<OcNote> getOcNotes(String username, String password);

    void updateId(int oldId, int newId);

    Optional<OcNote> findOcNoteByContent(String content);
}