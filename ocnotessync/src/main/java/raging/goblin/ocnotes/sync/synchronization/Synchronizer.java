/*
 * Copyright 2015, RagingGoblin <http://raginggoblin.wordpress.com>
 *
 * This file is part of OcNotes.
 *
 *  OcNotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OcNotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OcNotes.  If not, see <http://www.gnu.org/licenses/>.
 */
package raging.goblin.ocnotes.sync.synchronization;

import static java.util.function.Function.identity;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import raging.goblin.ocnotes.sync.log.LogLevel;
import raging.goblin.ocnotes.sync.log.Logger;
import raging.goblin.ocnotes.sync.webserviceclient.WebserviceException;

/**
 * The Synchronizer synchronizes two {@link OcNotesStorage} objects with each other.
 */
@AllArgsConstructor
public class Synchronizer {

    private Logger logger;
    private OcNotesStorage localStorage;
    private OcNotesStorage remoteStorage;
    private String username;
    private String password;
    private SynchronizationProgressListener progressListener;

    public void synchronize(long lastSyncTime) throws WebserviceException {

        Function<Collection<OcNote>, Map<Integer, OcNote>> createMap = ocNotes -> ocNotes.stream()
                .map(note -> new OcNote(note))
                .collect(Collectors.toMap(OcNote::getId, identity()));

        Map<Integer, OcNote> localNotes = createMap.apply(localStorage.getOcNotes(username, password));
        progressListener.setProgress(5);

        Map<Integer, OcNote> originalLocalNotes = createMap.apply(localNotes.values());
        progressListener.setProgress(10);

        Map<Integer, OcNote> remoteNotes = createMap.apply(remoteStorage.getOcNotes(username, password));
        progressListener.setProgress(25);

        Map<Integer, OcNote> originalRemoteNotes = createMap.apply(remoteNotes.values());
        progressListener.setProgress(30);

        Set.copyOf(remoteNotes.keySet()).forEach(remoteId -> {
            if (localNotes.containsKey(remoteId)) {
                update(remoteId, localNotes, remoteNotes, lastSyncTime);
            } else {
                createOrRemove(remoteId, localNotes, remoteNotes, lastSyncTime);
            }
        });
        progressListener.setProgress(40);

        Set.copyOf(localNotes.keySet()).stream()
                .filter(localId -> !remoteNotes.containsKey(localId))
                .forEach(localId -> createOrRemove(localId, remoteNotes, localNotes, lastSyncTime));
        progressListener.setProgress(50);

        updateStorage(localStorage, localNotes, originalLocalNotes, username, password, false);
        progressListener.setProgress(60);

        updateStorage(remoteStorage, remoteNotes, originalRemoteNotes, username, password, true);
        progressListener.setProgress(90);
    }

    private void createOrRemove(final Integer missingId, final Map<Integer, OcNote> collectionWithoutId, final Map<Integer, OcNote> collectionWithId, long lastSyncTime) {
        OcNote missingNote = collectionWithId.get(missingId);
        if (missingNote.getModified() < lastSyncTime) {
            logger.log(LogLevel.DEBUG, "Note to remove: " + missingNote);
            collectionWithId.remove(missingId);
        } else {
            logger.log(LogLevel.DEBUG, "Note to add: " + missingNote);
            collectionWithoutId.put(missingId, missingNote);
        }
    }

    private void update(Integer id, Map<Integer, OcNote> localNotes, Map<Integer, OcNote> remoteNotes, long lastSyncTime) {
        OcNote localNote = localNotes.get(id);
        OcNote remoteNote = remoteNotes.get(id);
        if (!localNote.equals(remoteNote)) {
            if (remoteNote.getModified() >= lastSyncTime) {
                logger.log(LogLevel.DEBUG, "Local note to update from " + localNote + " to " + remoteNote);
                localNote.update(remoteNote);
            } else if (localNote.getModified() >= lastSyncTime) {
                logger.log(LogLevel.DEBUG, "Remote note to update from " + remoteNote + " to " + localNote);
                remoteNote.update(localNote);
            } else {
                logger.log(LogLevel.DEBUG, "Local note to update from " + localNote + " to " + remoteNote);
                localNote.update(remoteNote);
            }
        }
    }

    private void updateStorage(final OcNotesStorage storageToUpdate, final Map<Integer, OcNote> synchronizedNotes, final Map<Integer, OcNote> originalNotes, String username, String password, boolean isExternal) {
        List<OcNote> toAdd = synchronizedNotes.values().stream()
                .filter(note -> !originalNotes.containsKey(note.getId()))
                .collect(Collectors.toList());
        logger.log(LogLevel.DEBUG, "Add these notes: " + toAdd);
        List<OcNote> addedOcNotes = storageToUpdate.addOcNotes(toAdd, username, password);

        if (isExternal && !addedOcNotes.isEmpty()) {
            updateIds(username, password, addedOcNotes);
        }

        List<OcNote> toUpdate = synchronizedNotes.values().stream()
                .filter(note -> originalNotes.containsKey(note.getId()))
                .filter(note -> !originalNotes.get(note.getId()).equals(note))
                .collect(Collectors.toList());
        logger.log(LogLevel.DEBUG, "Update these notes: " + toUpdate);
        storageToUpdate.updateOcNotes(toUpdate, username, password);

        List<OcNote> toRemove = originalNotes.values().stream()
                .filter(note -> !synchronizedNotes.containsKey(note.getId()))
                .collect(Collectors.toList());
        logger.log(LogLevel.DEBUG, "Remove these notes: " + toRemove);
        storageToUpdate.removeOcNotes(toRemove, username, password);
    }

    private void updateIds(final String username, final String password, final List<OcNote> addedOcNotes) {
        addedOcNotes.forEach(note -> {
            Optional<OcNote> toUpdateId = localStorage.findOcNoteByContent(note.getContent());
            if (toUpdateId.isPresent()) {
                localStorage.updateId(toUpdateId.get().getId(), note.getId());
            } else {
                localStorage.addOcNotes(List.of(note), username, password);
            }
        });
    }
}
