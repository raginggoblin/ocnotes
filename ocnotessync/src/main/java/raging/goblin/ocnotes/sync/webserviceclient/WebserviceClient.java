/*
 * Copyright 2015, RagingGoblin <http://raginggoblin.wordpress.com>
 *
 * This file is part of OcNotes.
 *
 *  OcNotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OcNotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OcNotes.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.ocnotes.sync.webserviceclient;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import raging.goblin.ocnotes.sync.synchronization.OcNote;
import raging.goblin.ocnotes.sync.synchronization.OcNotesStorage;

@RequiredArgsConstructor
public class WebserviceClient implements OcNotesStorage {

    private final String ipAddress;
    private final String folder;
    private final int port;
    private final boolean useHttps;

    private ObjectMapper mapper = new ObjectMapper();
    private HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_2)
            .build();

    public List<OcNote> getOcNotes(String username, String password) throws WebserviceException {
        try {
            return getOcNotesThrowingExceptions(username, password);
        } catch (WebserviceException | InterruptedException | IOException e) {
            throw new WebserviceException(e.getMessage());
        }
    }

    public List<OcNote> addOcNotes(List<OcNote> ocNotes, String username, String password) throws WebserviceException {
        Optional<Error> errorRaised = Optional.empty();
        List<OcNote> result = new ArrayList<>();

        for (OcNote ocNote : ocNotes) {
            try {
                result.add(addOcNote(ocNote, username, password));
            } catch (WebserviceException | InterruptedException | IOException e) {
                errorRaised = Optional.of(Error.builder()
                        .message(String.format("Failed to add OcNote: ", ocNote.toString()))
                        .cause(e)
                        .build());
            }
        }

        errorRaised.ifPresent(error -> {
            throw new WebserviceException(error.getMessage(), error.getCause());
        });

        return result;
    }

    public void removeOcNotes(List<OcNote> ocNotes, String username, String password) throws WebserviceException {
        Optional<Error> errorRaised = Optional.empty();
        for (OcNote ocNote : ocNotes) {
            try {
                removeOcNote(ocNote, username, password);
            } catch (WebserviceException | IOException | InterruptedException e) {
                errorRaised = Optional.of(Error.builder()
                        .message(String.format("Failed to remove OcNote: ", ocNote.toString()))
                        .cause(e)
                        .build());
            }
        }

        errorRaised.ifPresent(error -> {
            throw new WebserviceException(error.getMessage(), error.getCause());
        });
    }

    public List<OcNote> updateOcNotes(List<OcNote> ocNotes, String username, String password) throws WebserviceException {
        Optional<Error> errorRaised = Optional.empty();
        List<OcNote> result = new ArrayList<>();

        for (OcNote ocNote : ocNotes) {
            try {
                result.add(updateOcNote(ocNote, username, password));
            } catch (WebserviceException | InterruptedException | IOException e) {
                errorRaised = Optional.of(Error.builder()
                        .message(String.format("Failed to update OcNote: ", ocNote.toString()))
                        .cause(e)
                        .build());
            }
        }

        errorRaised.ifPresent(error -> {
            throw new WebserviceException(error.getMessage(), error.getCause());
        });

        return result;
    }

    public void updateId(int oldId, int newId) {
        // Nothing to do Ids are created on the server
    }

    public Optional<OcNote> findOcNoteByContent(final String content) {
        // Nothing to return as this is only used on the client
        return Optional.empty();
    }

    void setHttpClient(HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    private List<OcNote> getOcNotesThrowingExceptions(String username, String password) throws IOException, InterruptedException, WebserviceException {
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(uri()))
                .header("Authorization", basicAuth(username, password))
                .build();
        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        if (response.statusCode() != 200) {
            throw new WebserviceException(String.format("Failed to get OcNotes, HTTP response code: %d", response.statusCode()));
        }

        return Arrays.asList(mapper.readValue(response.body(), OcNote[].class));
    }

    private OcNote addOcNote(OcNote ocNote, String username, String password) throws IOException, InterruptedException, WebserviceException {
        final String json = mapper.writeValueAsString(ocNote);
        HttpRequest request = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString(json))
                .uri(URI.create(uri()))
                .header("Authorization", basicAuth(username, password))
                .header("Accept", "application/json")
                .build();
        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        if (response.statusCode() != 200) {
            throw new WebserviceException(String.format("Failed to add OcNote: %s, HTTP response code: %d", ocNote.toString(), response.statusCode()));
        }

        return mapper.readValue(response.body(), OcNote.class);
    }

    private void removeOcNote(OcNote ocNote, String username, String password) throws IOException, InterruptedException, WebserviceException {
        HttpRequest request = HttpRequest.newBuilder()
                .DELETE()
                .uri(URI.create(uri() + "/" + ocNote.getId()))
                .header("Authorization", basicAuth(username, password))
                .build();
        HttpResponse<Void> response = httpClient.send(request, HttpResponse.BodyHandlers.discarding());

        if (response.statusCode() != 200) {
            throw new WebserviceException(String.format("Failed to remove OcNote: %s, HTTP response code: %d", ocNote.toString(), response.statusCode()));
        }
    }

    private OcNote updateOcNote(OcNote ocNote, String username, String password) throws IOException, InterruptedException {
        final String json = mapper.writeValueAsString(ocNote);
        HttpRequest request = HttpRequest.newBuilder()
                .PUT(HttpRequest.BodyPublishers.ofString(json))
                .uri(URI.create(uri() + "/" + ocNote.getId()))
                .header("Authorization", basicAuth(username, password))
                .header("Accept", "application/json")
                .build();
        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        if (response.statusCode() != 200) {
            throw new WebserviceException(String.format("Failed to update OcNote: %s, HTTP response code: %d", ocNote.toString(), response.statusCode()));
        }

        return mapper.readValue(response.body(), OcNote.class);
    }

    private String uri() {
        return String.format("http%s://%s:%d/%s/index.php/apps/notes/api/v0.2/notes",
                useHttps ? "s" : "",
                ipAddress,
                port,
                folder);
    }

    private String basicAuth(String username, String password) {
        return "Basic " + Base64.getEncoder().encodeToString((username + ":" + password).getBytes());
    }

    @Data
    @Builder
    private static class Error {
        private String message;
        private Exception cause;
    }
}
