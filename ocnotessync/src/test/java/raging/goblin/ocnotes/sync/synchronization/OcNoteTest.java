/*
 * Copyright 2020, RagingGoblin <http://raginggoblin.wordpress.com>
 *
 * This file is part of OcNotes.
 *
 *  OcNotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OcNotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OcNotes.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.ocnotes.sync.synchronization;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;

import org.assertj.core.data.Offset;
import org.junit.jupiter.api.Test;

import raging.goblin.ocnotes.sync.synchronization.OcNote;

public class OcNoteTest {

    @Test
    public void constructById() {
        final OcNote hundred = new OcNote(100);

        assertThat(hundred.getTitle()).isEqualTo("New note");
        assertThat(hundred.getContent()).isEqualTo("New note");
        assertThat(hundred.getId()).isEqualTo(100);
        assertThat(hundred.getCategory()).isEqualTo("");
        assertThat(hundred.isFavorite()).isEqualTo(false);
        assertThat(hundred.getModified()).isCloseTo(Instant.now().getEpochSecond(), Offset.offset(10L));
    }

    @Test
    public void getTitle() {
        assertThat(singleLine().getTitle()).isEqualTo("This is the first line");
        assertThat(multipleLines().getTitle()).isEqualTo("This is the first line");
    }

    @Test
    public void getContent() {
        assertThat(singleLine().getContent()).isEqualTo("This is the first line");
        assertThat(multipleLines().getContent()).isEqualTo("This is the first line\nThis is the second line\nThis is the third line");
    }

    @Test
    public void getValue() {
        assertThat(singleLine().getValue()).isEqualTo("");
        assertThat(multipleLines().getValue()).isEqualTo("This is the second line\nThis is the third line");
    }

    @Test
    public void update() {
        final OcNote complete = OcNote.builder()
                .id(500)
                .content("New content")
                .category("Category")
                .favorite(true)
                .modified(500L)
                .build();

        complete.update(singleLine());

        assertThat(complete).isEqualTo(singleLine());
    }

    private OcNote singleLine() {
        return OcNote.builder()
                .content("This is the first line")
                .build();
    }

    private OcNote multipleLines() {
        return OcNote.builder()
                .content("This is the first line\nThis is the second line\nThis is the third line")
                .build();
    }
}
