/*
 * Copyright 2020, RagingGoblin <http://raginggoblin.wordpress.com>
 *
 * This file is part of OcNotes.
 *
 *  OcNotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OcNotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OcNotes.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.ocnotes.sync.synchronization;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import lombok.Data;
import raging.goblin.ocnotes.sync.log.Logger;
import raging.goblin.ocnotes.sync.synchronization.OcNote;
import raging.goblin.ocnotes.sync.synchronization.OcNotesStorage;
import raging.goblin.ocnotes.sync.synchronization.SynchronizationProgressListener;
import raging.goblin.ocnotes.sync.synchronization.Synchronizer;

public class SynchronizerTest {

    private LocalStorage localStorage;
    private RemoteStorage remoteStorage;
    private long lastModified;

    @BeforeEach
    public void setup() {
        localStorage = new LocalStorage();
        remoteStorage = new RemoteStorage();
        lastModified = Instant.now().getEpochSecond();
    }

    @Test
    public void addNotes() {
        localStorage.addOcNotes(List.of(OcNote.builder()
                .id(-1)
                .modified(Instant.now().getEpochSecond())
                .category("Cat1")
                .content("Local 1")
                .favorite(true)
                .build()), "", "");
        assertThat(localStorage.getOcNotes("", "")).isNotEqualTo(remoteStorage.getOcNotes("", ""));

        synchronize();
        assertThat(localStorage.getOcNotes("", "")).isEqualTo(remoteStorage.getOcNotes("", ""));
        assertThat(localStorage.getOcNotes("", "").size()).isEqualTo(1);

        remoteStorage.addOcNotes(List.of(OcNote.builder()
                .id(5)
                .modified(Instant.now().getEpochSecond())
                .category("Cat2")
                .content("Remote 1")
                .favorite(false)
                .build()), "", "");
        assertThat(localStorage.getOcNotes("", "")).isNotEqualTo(remoteStorage.getOcNotes("", ""));

        synchronize();
        assertThat(localStorage.getOcNotes("", "")).isEqualTo(remoteStorage.getOcNotes("", ""));
        assertThat(localStorage.getOcNotes("", "").size()).isEqualTo(2);

        localStorage.addOcNotes(List.of(OcNote.builder()
                .id(-2)
                .modified(Instant.now().getEpochSecond())
                .category("Cat1")
                .content("Local 2")
                .favorite(true)
                .build()), "", "");
        remoteStorage.addOcNotes(List.of(OcNote.builder()
                .id(6)
                .modified(Instant.now().getEpochSecond())
                .category("Cat2")
                .content("Remote 2")
                .favorite(false)
                .build()), "", "");
        assertThat(localStorage.getOcNotes("", "")).isNotEqualTo(remoteStorage.getOcNotes("", ""));

        synchronize();
        assertThat(localStorage.getOcNotes("", "")).isEqualTo(remoteStorage.getOcNotes("", ""));
        assertThat(localStorage.getOcNotes("", "").size()).isEqualTo(4);
    }

    @Test
    public void modifyNotes() {
        OcNote note1 = OcNote.builder()
                .id(1)
                .modified(Instant.now().getEpochSecond())
                .category("Cat1")
                .content("Remote 1")
                .favorite(true)
                .build();
        OcNote note2 = OcNote.builder()
                .id(2)
                .modified(Instant.now().getEpochSecond())
                .category("Cat1")
                .content("Remote 2")
                .favorite(true)
                .build();
        remoteStorage.addOcNotes(List.of(note1, note2), "", "");
        assertThat(localStorage.getOcNotes("", "")).isNotEqualTo(remoteStorage.getOcNotes("", ""));

        synchronize();
        note1.setId(10000);
        note2.setId(10001);
        assertThat(localStorage.getOcNotes("", "")).isEqualTo(remoteStorage.getOcNotes("", ""));
        assertThat(localStorage.getOcNotes("", "").size()).isEqualTo(2);

        note1.setContent("Aap");
        note1.setModified(Instant.now().getEpochSecond());
        remoteStorage.updateOcNotes(List.of(note1), "", "");
        assertThat(localStorage.getOcNotes("", "")).isNotEqualTo(remoteStorage.getOcNotes("", ""));

        synchronize();
        assertThat(localStorage.getOcNotes("", "")).isEqualTo(remoteStorage.getOcNotes("", ""));
        assertThat(localStorage.getOcNotes("", "").size()).isEqualTo(2);

        note1.setContent("Noot");
        note1.setModified(Instant.now().getEpochSecond());
        localStorage.updateOcNotes(List.of(note1), "", "");
        assertThat(localStorage.getOcNotes("", "")).isNotEqualTo(remoteStorage.getOcNotes("", ""));

        synchronize();
        assertThat(localStorage.getOcNotes("", "")).isEqualTo(remoteStorage.getOcNotes("", ""));
        assertThat(localStorage.getOcNotes("", "").size()).isEqualTo(2);

        note1.setContent("Mies");
        note1.setModified(Instant.now().getEpochSecond());
        remoteStorage.updateOcNotes(List.of(note1), "", "");
        note2.setContent("Roos");
        note2.setModified(Instant.now().getEpochSecond());
        localStorage.updateOcNotes(List.of(note2), "", "");
        assertThat(localStorage.getOcNotes("", "")).isNotEqualTo(remoteStorage.getOcNotes("", ""));

        synchronize();
        assertThat(localStorage.getOcNotes("", "")).isEqualTo(remoteStorage.getOcNotes("", ""));
        assertThat(localStorage.getOcNotes("", "").size()).isEqualTo(2);

        note1.setContent("Boom");
        note1.setModified(Instant.now().getEpochSecond());
        remoteStorage.updateOcNotes(List.of(note1), "", "");
        note1.setContent("Vis");
        note1.setModified(Instant.now().getEpochSecond());
        localStorage.updateOcNotes(List.of(note2), "", "");
        assertThat(localStorage.getOcNotes("", "")).isNotEqualTo(remoteStorage.getOcNotes("", ""));

        synchronize();
        assertThat(localStorage.getOcNotes("", "")).isEqualTo(remoteStorage.getOcNotes("", ""));
        assertThat(localStorage.getOcNotes("", "").size()).isEqualTo(2);
        assertThat(localStorage.getNotes().get(10000).getContent()).isEqualTo("Boom");
        assertThat(localStorage.getNotes().get(10001).getContent()).isEqualTo("Remote 2");
    }

    @Test
    public void removeNotes() {
        OcNote note1 = OcNote.builder()
                .id(1)
                .modified(Instant.now().getEpochSecond())
                .category("Cat1")
                .content("Remote 1")
                .favorite(true)
                .build();
        OcNote note2 = OcNote.builder()
                .id(2)
                .modified(Instant.now().getEpochSecond())
                .category("Cat1")
                .content("Remote 2")
                .favorite(true)
                .build();
        OcNote note3 = OcNote.builder()
                .id(3)
                .modified(Instant.now().getEpochSecond())
                .category("Cat1")
                .content("Remote 3")
                .favorite(true)
                .build();
        remoteStorage.addOcNotes(List.of(note1, note2, note3), "", "");
        assertThat(localStorage.getOcNotes("", "")).isNotEqualTo(remoteStorage.getOcNotes("", ""));

        synchronize();
        note1.setId(10000);
        note2.setId(10001);
        note3.setId(10002);
        assertThat(localStorage.getOcNotes("", "")).isEqualTo(remoteStorage.getOcNotes("", ""));
        assertThat(localStorage.getOcNotes("", "").size()).isEqualTo(3);

        remoteStorage.removeOcNotes(List.of(note1), "", "");
        assertThat(localStorage.getOcNotes("", "")).isNotEqualTo(remoteStorage.getOcNotes("", ""));

        lastModified = Instant.now().getEpochSecond() + 10;
        synchronize();
        assertThat(localStorage.getOcNotes("", "")).isEqualTo(remoteStorage.getOcNotes("", ""));
        assertThat(localStorage.getOcNotes("", "").size()).isEqualTo(2);

        localStorage.removeOcNotes(List.of(note2), "", "");
        assertThat(localStorage.getOcNotes("", "")).isNotEqualTo(remoteStorage.getOcNotes("", ""));

        lastModified = Instant.now().getEpochSecond() + 10;
        synchronize();
        assertThat(localStorage.getOcNotes("", "")).isEqualTo(remoteStorage.getOcNotes("", ""));
        assertThat(localStorage.getOcNotes("", "").size()).isEqualTo(1);

        localStorage.removeOcNotes(List.of(note3), "", "");
        remoteStorage.removeOcNotes(List.of(note3), "", "");

        lastModified = Instant.now().getEpochSecond() + 10;
        synchronize();
        assertThat(localStorage.getOcNotes("", "")).isEqualTo(remoteStorage.getOcNotes("", ""));
        assertThat(localStorage.getOcNotes("", "").size()).isEqualTo(0);
    }

    private void synchronize() {
        Synchronizer synchronizer = new Synchronizer(new Logger(){}, localStorage, remoteStorage, "", "", new CommandLineProgressListener());
        synchronizer.synchronize(lastModified);
        lastModified = Instant.now().getEpochSecond();
    }

    @Data
    private abstract static class TestOcNotesStorage implements OcNotesStorage {

        protected Map<Integer, OcNote> notes = new HashMap<>();

        @Override
        public void removeOcNotes(final List<OcNote> notes, final String username, final String password) {
            notes.forEach(note -> this.notes.remove(note.getId()));
        }

        @Override
        public List<OcNote> getOcNotes(final String username, final String password) {
            return new ArrayList<>(notes.values());
        }

        @Override
        public void updateId(final int oldId, final int newId) {
            OcNote note = notes.remove(oldId);
            note.setId(newId);
            notes.put(newId, note);
        }
    }

    private static class LocalStorage extends TestOcNotesStorage {

        @Override
        public List<OcNote> addOcNotes(final List<OcNote> notes, final String username, final String password) {
            notes.forEach(note -> this.notes.put(note.getId(), note));
            return getOcNotes(username, password);
        }

        @Override
        public Optional<OcNote> findOcNoteByContent(final String content) {
            return notes.values().stream()
                    .filter(note -> note.getContent().equals(content))
                    .findFirst();
        }

        @Override
        public List<OcNote> updateOcNotes(final List<OcNote> notes, final String username, final String password) {
            notes.forEach(note -> this.notes.get(note.getId()).update(note));
            return notes;
        }
    }

    private static class RemoteStorage extends TestOcNotesStorage {

        int idCounter = 10000;

        @Override
        public List<OcNote> addOcNotes(final List<OcNote> notes, final String username, final String password) {
            return notes.stream()
                    .map(note -> {
                        int id = idCounter++;
                        OcNote newNote = new OcNote(note);
                        newNote.setId(id);
                        this.notes.put(id, newNote);
                        return this.notes.get(id);
                    })
                    .collect(Collectors.toList());
        }

        @Override
        public Optional<OcNote> findOcNoteByContent(final String content) {
            return Optional.empty();
        }

        @Override
        public List<OcNote> updateOcNotes(final List<OcNote> notes, final String username, final String password) {
            notes.forEach(note -> {
                this.notes.remove(note.getId());
                this.notes.put(note.getId(), new OcNote(note));
            });
            return getOcNotes(username, password);
        }
    }

    private class CommandLineProgressListener implements SynchronizationProgressListener {

        @Override
        public void setProgress(final int percentage) {
            System.out.println("Progress: " + percentage);
        }
    }
}
