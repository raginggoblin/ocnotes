/*
 * Copyright 2020, RagingGoblin <http://raginggoblin.wordpress.com>
 *
 * This file is part of OcNotes.
 *
 *  OcNotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OcNotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OcNotes.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.ocnotes.sync.webserviceclient;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpResponse;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import raging.goblin.ocnotes.sync.synchronization.OcNote;

public class WebserviceClientTest {

    private HttpClient httpClient;
    private WebserviceClient webserviceClient;
    private HttpResponse httpResponse;

    @BeforeEach
    public void init() {
        httpClient = mock(HttpClient.class);
        webserviceClient = new WebserviceClient("raging.goblin.ocnotes", "nextcloud", 443, true);
        webserviceClient.setHttpClient(httpClient);
        httpResponse = mock(HttpResponse.class);
    }

    @Test
    public void getOcNotes() throws IOException, InterruptedException, WebserviceException {
        when(httpResponse.statusCode()).thenReturn(200);
        when(httpResponse.body()).thenReturn("[" + responseBody() + "]");
        when(httpClient.send(any(), any())).thenReturn(httpResponse);

        assertThat(webserviceClient.getOcNotes("un", "pw")).isEqualTo(List.of(ocNote()));
    }

    @Test
    public void getOcNotesHttp500() throws IOException, InterruptedException {
        when(httpResponse.statusCode()).thenReturn(500);
        when(httpClient.send(any(), any())).thenReturn(httpResponse);

        assertThatThrownBy(() -> webserviceClient.getOcNotes("un", "pw")).isInstanceOf(WebserviceException.class);
    }

    @Test
    public void getOcNotesException() throws IOException, InterruptedException {
        when(httpClient.send(any(), any())).thenThrow(new IOException("FOUT"));

        assertThatThrownBy(() -> webserviceClient.getOcNotes("un", "pw")).isInstanceOf(WebserviceException.class);
    }

    @Test
    public void addOcNotes() throws IOException, InterruptedException, WebserviceException {
        when(httpResponse.statusCode()).thenReturn(200);
        when(httpResponse.body()).thenReturn(responseBody());
        when(httpClient.send(any(), any())).thenReturn(httpResponse);

        assertThat(webserviceClient.addOcNotes(List.of(ocNote()), "un", "pw")).isEqualTo(List.of(ocNote()));
    }

    @Test
    public void addOcNotesHttp500() throws IOException, InterruptedException {
        when(httpResponse.statusCode()).thenReturn(500);
        when(httpClient.send(any(), any())).thenReturn(httpResponse);

        assertThatThrownBy(() -> webserviceClient.addOcNotes(List.of(ocNote()), "un", "pw")).isInstanceOf(WebserviceException.class);
    }


    @Test
    public void addOcNotesException() throws IOException, InterruptedException {
        when(httpClient.send(any(), any())).thenThrow(new IOException("FOUT"));

        assertThatThrownBy(() -> webserviceClient.addOcNotes(List.of(ocNote()), "un", "pw")).isInstanceOf(WebserviceException.class);
    }

    @Test
    public void removeOcNotes() throws IOException, InterruptedException, WebserviceException {
        when(httpResponse.statusCode()).thenReturn(200);
        when(httpResponse.body()).thenReturn(null);
        when(httpClient.send(any(), any())).thenReturn(httpResponse);

        webserviceClient.removeOcNotes(List.of(ocNote()), "un", "pw");
    }

    @Test
    public void removecNotesHttp500() throws IOException, InterruptedException {
        when(httpResponse.statusCode()).thenReturn(500);
        when(httpClient.send(any(), any())).thenReturn(httpResponse);

        assertThatThrownBy(() -> webserviceClient.removeOcNotes(List.of(ocNote()), "un", "pw")).isInstanceOf(WebserviceException.class);
    }


    @Test
    public void removeOcNotesException() throws IOException, InterruptedException {
        when(httpClient.send(any(), any())).thenThrow(new IOException("FOUT"));

        assertThatThrownBy(() -> webserviceClient.removeOcNotes(List.of(ocNote()), "un", "pw")).isInstanceOf(WebserviceException.class);
    }


    @Test
    public void updateOcNotes() throws IOException, InterruptedException, WebserviceException {
        when(httpResponse.statusCode()).thenReturn(200);
        when(httpResponse.body()).thenReturn(responseBody());
        when(httpClient.send(any(), any())).thenReturn(httpResponse);

        assertThat(webserviceClient.updateOcNotes(List.of(ocNote()), "un", "pw")).isEqualTo(List.of(ocNote()));
    }

    @Test
    public void updateOcNotesHttp500() throws IOException, InterruptedException {
        when(httpResponse.statusCode()).thenReturn(500);
        when(httpClient.send(any(), any())).thenReturn(httpResponse);

        assertThatThrownBy(() -> webserviceClient.updateOcNotes(List.of(ocNote()), "un", "pw")).isInstanceOf(WebserviceException.class);
    }


    @Test
    public void updateOcNotesException() throws IOException, InterruptedException {
        when(httpClient.send(any(), any())).thenThrow(new IOException("FOUT"));

        assertThatThrownBy(() -> webserviceClient.updateOcNotes(List.of(ocNote()), "un", "pw")).isInstanceOf(WebserviceException.class);
    }

    private OcNote ocNote() {
        return OcNote.builder()
                .modified(1572706614)
                .category("")
                .content("Title\nBody line 1\nBody line 2")
                .favorite(false)
                .id(295376)
                .build();
    }

    private String responseBody() {
        return "    {\n" +
                "        \"etag\": \"919af2038a7580fbf97248d4fb8e05e9\",\n" +
                "        \"modified\": 1572706614,\n" +
                "        \"title\": \"Title\",\n" +
                "        \"category\": \"\",\n" +
                "        \"content\": \"Title\\nBody line 1\\nBody line 2\",\n" +
                "        \"favorite\": false,\n" +
                "        \"error\": false,\n" +
                "        \"errorMessage\": \"\",\n" +
                "        \"id\": 295376\n" +
                "    }";
    }
}
