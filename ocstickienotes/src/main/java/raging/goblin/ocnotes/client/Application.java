///*
// * Copyright 2015, RagingGoblin <http://raginggoblin.wordpress.com>
// *
// * This file is part of OcNotes.
// *
// *  OcNotes is free software: you can redistribute it and/or modify
// *  it under the terms of the GNU General Public License as published by
// *  the Free Software Foundation, either version 3 of the License, or
// *  (at your option) any later version.
// *
// *  OcNotes is distributed in the hope that it will be useful,
// *  but WITHOUT ANY WARRANTY; without even the implied warranty of
// *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// *  GNU General Public License for more details.
// *
// *  You should have received a copy of the GNU General Public License
// *  along with OcNotes.  If not, see <http://www.gnu.org/licenses/>.
// */
//
//package raging.goblin.ocnotes.client;
//
//import java.io.File;
//import java.io.IOException;
//import java.io.InputStream;
//import java.nio.file.Files;
//import java.nio.file.Paths;
//import java.util.Locale;
//
//import javax.swing.*;
//import javax.swing.UIManager.LookAndFeelInfo;
//
//import raging.goblin.ocnotes.client.data.StickyNotesStorage;
//import raging.goblin.ocnotes.client.gui.ClientGui;
//import raging.goblin.ocnotes.client.gui.HelpDialog;
//import raging.goblin.ocnotes.client.properties.OcNotesProperties;
//import raging.goblin.ocnotes.sync.log.LogLevel;
//import raging.goblin.ocnotes.sync.log.Logger;
//
//public class Application {
//
//    private static final Logger LOG = new Log4jLogger(Application.class);
//    private static final OcNotesProperties PROPERTIES = OcNotesProperties.getInstance();
//
//    public static void main(String[] args) {
//        System.setProperty("awt.useSystemAAFontSettings", "on");
//        System.setProperty("swing.aatext", "true");
//
//        setLocale();
//        loadLaf();
//        copyDefaultIcon();
//
//        StickyNotesStorage localStorage = new StickyNotesStorage();
//
//        SynchronizationHandler synchronizationHandler = new SynchronizationHandler(localStorage);
//        if (PROPERTIES.isSynchronizationEnabled()) {
//            synchronizationHandler.start();
//        }
//
//        Runtime.getRuntime().addShutdownHook(new ShutdownHook(synchronizationHandler));
//
//        checkFirstRun();
//
//        new ClientGui(localStorage, synchronizationHandler);
//    }
//
//    public static void loadLaf() {
//        try {
//            boolean isLinux = tryLinuxLaf();
//            if (!isLinux) {
//                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
//                LOG.log(LogLevel.DEBUG, "Setting system look and feel: " + UIManager.getSystemLookAndFeelClassName());
//            }
//        } catch (Exception e) {
//            LOG.log(LogLevel.ERROR, "Could not set system look and feel");
//            LOG.log(LogLevel.DEBUG, e.getMessage(), e);
//        }
//    }
//
//    private static void setLocale() {
//        Locale.setDefault(new Locale(PROPERTIES.getLanguage()));
//    }
//
//    private static boolean tryLinuxLaf() throws ClassNotFoundException, InstantiationException, IllegalAccessException,
//            UnsupportedLookAndFeelException {
//        for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
//            if ("GTK+".equals(info.getName())) {
//                UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
//                LOG.log(LogLevel.DEBUG, "Setting GTK+ look and feel");
//                return true;
//            }
//        }
//        return false;
//    }
//
//    private static void checkFirstRun() {
//        if (PROPERTIES.isFirstRun()) {
//            SwingUtilities.invokeLater(() -> {
//                HelpDialog helpFrame = new HelpDialog(null);
//                helpFrame.setVisible(true);
//            });
//            PROPERTIES.setFirstRun(false);
//        }
//    }
//
//    private static void copyDefaultIcon() {
//        if (!new File(PROPERTIES.getTrayIcon()).exists()) {
//            InputStream iconStream = Application.class.getResourceAsStream("/icons/notes.png");
//            try {
//                Files.copy(iconStream, Paths.get(PROPERTIES.getTrayIcon()));
//            } catch (IOException e) {
//                LOG.log(LogLevel.ERROR, "Unable to create notes icon", e);
//            }
//        }
//    }
//
//    /**
//     * This class provides a shutdownhook according to:
//     *
//     * <a href="http://onjava.com/pub/a/onjava/2003/03/26/shutdownhook.html"> http://onjava.com/pub/a/onjava/2003/03/26/
//     * shutdownhook.html</a>
//     */
//    private static class ShutdownHook extends Thread {
//
//        private SynchronizationHandler synchronizationHandler;
//
//        public ShutdownHook(SynchronizationHandler synchronizationHandler) {
//            this.synchronizationHandler = synchronizationHandler;
//        }
//
//        @Override
//        public void run() {
//            while (this.synchronizationHandler.isRunning()) {
//                this.synchronizationHandler.stop();
//                try {
//                    Thread.sleep(100);
//                } catch (InterruptedException e) {
//                    LOG.log(LogLevel.ERROR, "Unable to stop synchronization on shutdown", e);
//                }
//            }
//        }
//    }
//}
