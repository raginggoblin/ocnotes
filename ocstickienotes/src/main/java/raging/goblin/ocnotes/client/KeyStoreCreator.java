/*
 * Copyright 2015, RagingGoblin <http://raginggoblin.wordpress.com>
 *
 * This file is part of OcNotes.
 *
 *  OcNotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OcNotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OcNotes.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.ocnotes.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import javax.swing.*;

import raging.goblin.ocnotes.sync.log.LogLevel;
import raging.goblin.ocnotes.sync.log.Logger;

public class KeyStoreCreator {

    private static final Logger LOG = new LogForJLogger(KeyStoreCreator.class);

    private KeyStoreCreator() {
        // Utility class
    }

    public static boolean createKeyStore(String hostName, int port, File keystore, char[] passphrase)
            throws NoSuchAlgorithmException, UnknownHostException, IOException, KeyStoreException, CertificateException,
            KeyManagementException {

        KeyStore ks = loadSystemKeyStore();

        SSLContext context = SSLContext.getInstance("TLS");
        SavingTrustManager tm = createTrustManager(ks, context);

        SSLSocketFactory factory = context.getSocketFactory();
        LOG.log(LogLevel.DEBUG, "Opening connection to " + hostName + ":" + port);

        SSLSocket socket = (SSLSocket) factory.createSocket(hostName, port);
        socket.setSoTimeout(5000);
        boolean keyExists = false;

        try {
            LOG.log(LogLevel.DEBUG, "Starting SSL handshake");
            socket.startHandshake();
            socket.close();
            LOG.log(LogLevel.DEBUG, "No errors, certificate is already trusted");
            keyExists = true;
        } catch (SSLException e) {
            LOG.log(LogLevel.INFO, "Not a valid certificate (yet)");
            LOG.log(LogLevel.DEBUG, "Not a valid certificate (yet)", e);
        }

        if (!keyExists) {
            X509Certificate[] chain = tm.chain;
            if (chain == null) {
                LOG.log(LogLevel.ERROR, "Could not obtain server certificate chain");
                return false;
            }

            LOG.log(LogLevel.DEBUG, "Server sent " + chain.length + " certificate(s):");

            MessageDigest sha1 = MessageDigest.getInstance("SHA1");
            MessageDigest md5 = MessageDigest.getInstance("MD5");

            StringBuilder message = new StringBuilder("<html>");

            X509Certificate cert = chain[0];
            message.append("<table>");
            message.append("<tr><td><b>There is something wrong with the server certificate.</b></td></tr>");
            message.append("<tr><td></td></tr>");
            message.append("<tr><td><b>Issued to</b></td></tr>");
            message.append("<tr><td>" + cert.getSubjectDN() + "</td></tr>");
            message.append("<tr><td><b>Issued by</b></td></tr>");
            message.append("<tr><td>" + cert.getIssuerDN() + "</td></tr>");
            message.append("<tr><td></td></tr>");
            message.append("<tr><td><b>SHA1</b></td></tr>");
            sha1.update(cert.getEncoded());
            message.append("<tr><td>" + toHexString(sha1.digest()) + "</td></tr>");
            message.append("<tr><td><b>MD5</b></td></tr>");
            md5.update(cert.getEncoded());
            message.append("<tr><td>" + toHexString(md5.digest()) + "</td></tr>");
            message.append("<tr><td></td></tr>");
            message.append("<tr><td>Do you trust this certificate ?</td></tr>");
            message.append("</table>");
            message.append("</html>");

            int choise = JOptionPane.showConfirmDialog(null, message.toString(), "Confirm authenticity",
                    JOptionPane.YES_NO_OPTION);

            if (choise != 0) {
                JOptionPane.showMessageDialog(null, "Synchronization using https is not possible. \n\nyou have to accept the certificate or the system \nadministrator has to configure his system correctly.");
                return false;
            }

            String alias = hostName;
            ks.setCertificateEntry(alias, cert);
            LOG.log(LogLevel.DEBUG, cert.toString());
            LOG.log(LogLevel.DEBUG, "Added certificate to keystore using alias '" + alias + "'");
            OutputStream out = new FileOutputStream(keystore.getAbsolutePath());
            ks.store(out, passphrase);
            out.close();
            LOG.log(LogLevel.DEBUG, "Kestore saved to " + keystore.getAbsolutePath());
        }
        return true;
    }

    private static SavingTrustManager createTrustManager(KeyStore ks, SSLContext context)
            throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException {
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init(ks);
        X509TrustManager defaultTrustManager = (X509TrustManager) tmf.getTrustManagers()[0];
        SavingTrustManager tm = new SavingTrustManager(defaultTrustManager);
        context.init(null, new TrustManager[]{tm}, null);
        return tm;
    }

    private static KeyStore loadSystemKeyStore() throws IOException, NoSuchAlgorithmException, CertificateException,
            KeyStoreException {
        File file = new File("jssecacerts");
        if (file.isFile() == false) {
            char SEP = File.separatorChar;
            File dir = new File(System.getProperty("java.home") + SEP + "lib" + SEP + "security");
            file = new File(dir, "jssecacerts");
            if (file.isFile() == false) {
                file = new File(dir, "cacerts");
            }
        }
        LOG.log(LogLevel.DEBUG, "Loading KeyStore: " + file);

        InputStream in = null;
        in = new FileInputStream(file);
        KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
        ks.load(in, null);
        in.close();
        return ks;
    }

    private static final char[] HEXDIGITS = "0123456789abcdef".toCharArray();

    private static String toHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length * 3);
        for (int b : bytes) {
            b &= 0xff;
            sb.append(HEXDIGITS[b >> 4]);
            sb.append(HEXDIGITS[b & 15]);
            sb.append(' ');
        }
        return sb.toString();
    }

    private static class SavingTrustManager implements X509TrustManager {

        private final X509TrustManager tm;
        private X509Certificate[] chain;

        SavingTrustManager(X509TrustManager tm) {
            this.tm = tm;
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            throw new UnsupportedOperationException();
        }

        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            throw new UnsupportedOperationException();
        }

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            this.chain = chain;
            tm.checkServerTrusted(chain, authType);
        }
    }
}