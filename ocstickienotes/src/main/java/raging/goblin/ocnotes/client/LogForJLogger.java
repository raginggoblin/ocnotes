/*


 * Copyright 2015, RagingGoblin <http://raginggoblin.wordpress.com>
 * 
 * This file is part of OcNotes.
 *
 *  OcNotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OcNotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OcNotes.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.ocnotes.client;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import raging.goblin.ocnotes.sync.log.LogLevel;

public class LogForJLogger implements raging.goblin.ocnotes.sync.log.Logger {

	private Logger logger;

	public LogForJLogger(Class clazz) {
		this.logger = LogManager.getLogger(clazz);
	}

	@Override
	public void log(LogLevel level, String message, Exception e) {
		switch (level) {
		case DEBUG:
			logger.debug(message, e);
			break;
		case INFO:
			logger.info(message, e);
			break;
		case WARN:
			logger.warn(message, e);
			break;
		case ERROR:
		default:
			logger.error(message, e);
			break;
		}
	}

	@Override
	public void log(LogLevel level, String message) {
		log(level, message, null);
	}

}
