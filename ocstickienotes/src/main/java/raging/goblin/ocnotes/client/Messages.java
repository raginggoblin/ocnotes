/*
 * Copyright 2015, RagingGoblin <http://raginggoblin.wordpress.com>
 *
 * This file is part of OcNotes.
 *
 *  OcNotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OcNotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OcNotes.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.ocnotes.client;

import java.util.Locale;
import java.util.ResourceBundle;

import raging.goblin.ocnotes.client.properties.OcNotesProperties;

/**
 * Provides access to localized messages.
 */
public class Messages {

    private static final OcNotesProperties PROPERTIES = OcNotesProperties.getInstance();

    private static Messages instance;

    private ResourceBundle messages;

    private Messages() {
        Locale locale = new Locale(PROPERTIES.getLanguage(), PROPERTIES.getLanguage());
        messages = ResourceBundle.getBundle("messages", locale);
    }

    public static Messages getInstance() {
        if (instance == null) {
            instance = new Messages();
        }
        return instance;
    }

    public String get(String key) {
        return messages.getString(key);
    }
}
