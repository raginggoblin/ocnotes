/*
 * Copyright 2018, RagingGoblin <http://raginggoblin.wordpress.com>
 *
 * This file is part of OcNotes.
 *
 *  OcNotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OcNotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OcNotes.  If not, see <http://www.gnu.org/licenses/>.
 */
package raging.goblin.ocnotes.client;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;

import javax.swing.*;

import lombok.Getter;
import raging.goblin.ocnotes.client.data.StickyNotesStorage;
import raging.goblin.ocnotes.client.gui.ProgressDialog;
import raging.goblin.ocnotes.client.properties.OcNotesProperties;
import raging.goblin.ocnotes.sync.log.LogLevel;
import raging.goblin.ocnotes.sync.log.Logger;
import raging.goblin.ocnotes.sync.synchronization.OcNote;
import raging.goblin.ocnotes.sync.synchronization.Synchronizer;
import raging.goblin.ocnotes.sync.webserviceclient.WebserviceClient;
import raging.goblin.ocnotes.sync.webserviceclient.WebserviceException;

/**
 * The {@link SynchronizationHandler} synchronizes the local {@link OcNote} objects with the ones on the server on a
 * regular interval.
 */
public class SynchronizationHandler implements Runnable {

    private static final Logger LOG = new LogForJLogger(SynchronizationHandler.class);
    private static final OcNotesProperties PROPERTIES = OcNotesProperties.getInstance();

    @Getter
    private volatile boolean running = false;

    private StickyNotesStorage localStorage;
    private ProgressDialog progressDialog = new ProgressDialog(null, "");

    public SynchronizationHandler(StickyNotesStorage localStorage) {
        this.localStorage = localStorage;
    }

    public void start() {
        running = true;
        new Thread(this).start();
        LOG.log(LogLevel.INFO, "Synchronization started");
    }

    public void stop() {
        running = false;
        LOG.log(LogLevel.INFO, "Synchronization stopped");
    }

    public void synchronize(boolean showProgress) throws WebserviceException {
        LOG.log(LogLevel.INFO, "Synchronization start.");

        WebserviceClient remoteStorage = createRemoteStorage(PROPERTIES.getServerHostName(), PROPERTIES.getFolder(),
                PROPERTIES.getServerPort(), PROPERTIES.isHttps());

        if (remoteStorage != null) {
            try {
                if (showProgress) {
                    progressDialog.setProgress(0);
                    SwingUtilities.invokeLater(() -> progressDialog.setVisible(true));
                }

                Synchronizer synchronizer = new Synchronizer(new LogForJLogger(Synchronizer.class), localStorage,
                        remoteStorage, PROPERTIES.getUsername(), PROPERTIES.getPassword(), progressDialog);
                synchronizer.synchronize(PROPERTIES.getLastSyncTime());
                updateSyncInfo();

                if (showProgress) {
                    progressDialog.setProgress(100);
                    progressDialog.setVisible(false);
                }
            } catch (Exception e) {
                SwingUtilities.invokeLater(() -> progressDialog.setVisible(false));
                throw e;
            }

            LOG.log(LogLevel.INFO, "Synchronization complete, client updated.");
        }
    }

    public void resetSynchronization() {
        stop();

        for (int i = -localStorage.getStickyNotes().size(); i < 0; i++) {
            OcNote ocNote = localStorage.getStickyNotes().get(-1 - i).getData().getOcNote();
            OcNote newOcNote = new OcNote(i, ocNote.getModified(), ocNote.getContent());
            localStorage.getStickyNotes().get(-1 - i).getData().setOcNote(newOcNote, this);
        }
        PROPERTIES.setLastSyncTime(0);

        LOG.log(LogLevel.INFO, "Reset synchronization succeeded.");
    }

    public void resetFromServer() throws WebserviceException {
        stop();
        localStorage.getStickyNotes().forEach(sn -> sn.delete());
        PROPERTIES.setLastSyncTime(0);
        synchronize(true);

        LOG.log(LogLevel.INFO, "Reset from server succeeded.");

        if (PROPERTIES.isSynchronizationEnabled()) {
            start();
        }
    }

    public void resetFromClient() throws WebserviceException {
        stop();
        WebserviceClient remoteStorage = createRemoteStorage(PROPERTIES.getServerHostName(), PROPERTIES.getFolder(),
                PROPERTIES.getServerPort(), PROPERTIES.isHttps());

        if (remoteStorage != null) {
            List<OcNote> ocNotes = remoteStorage.getOcNotes(PROPERTIES.getUsername(), PROPERTIES.getPassword());
            remoteStorage.removeOcNotes(ocNotes, PROPERTIES.getUsername(), PROPERTIES.getPassword());
            PROPERTIES.setLastSyncTime(0);
            synchronize(true);
        }

        LOG.log(LogLevel.INFO, "Reset from client succeeded.");

        if (PROPERTIES.isSynchronizationEnabled()) {
            start();
        }
    }

    public boolean tryRemoteLogin(String host, String folder, Integer port, String username, String password,
                                  boolean https) {
        WebserviceClient remoteStorage = createRemoteStorage(host, folder, port, https);
        if (remoteStorage != null) {
            try {
                remoteStorage.getOcNotes(username, password);
            } catch (WebserviceException e) {
                LOG.log(LogLevel.ERROR, "Unable to login at remote storage", e);
                return false;
            }
        }
        LOG.log(LogLevel.DEBUG, "Successfuly logging in on remote server");
        return true;
    }

    @Override
    public void run() {
        while (running) {
            try {
                synchronize(false);
                sleep();
            } catch (Exception e) {
                LOG.log(LogLevel.ERROR, "Unable to synchronize, aborting synhronization...", e);
                running = false;
            }
        }
    }

    private void updateSyncInfo() {
        long lastSync = new Date().getTime() / 1000;
        PROPERTIES.setLastSyncTime(lastSync);
    }

    private void sleep() {
        try {
            Thread.sleep(PROPERTIES.getPollInterval() * 60 * 1000);
        } catch (InterruptedException e) {
            LOG.log(LogLevel.WARN, "Synchronizing interrupted");
            LOG.log(LogLevel.DEBUG, "RunnableSynchronizer interrupted", e);
        }
    }

    private WebserviceClient createRemoteStorage(String hostname, String folder, int port, boolean useHttps) {
        if (useHttps) {
            if (!loadKeyStore(hostname, port)) {
                return null;
            }
        }

        WebserviceClient remoteStorage = new WebserviceClient(hostname, folder, port, useHttps);
        return remoteStorage;
    }

    private boolean loadKeyStore(String hostName, int port) {
        File keyStore = new File(PROPERTIES.getKeystore());

        if (createKeyStore(hostName, port, keyStore, PROPERTIES.getPassphrase().toCharArray())) {
            System.setProperty("javax.net.ssl.trustStore", keyStore.getAbsolutePath());
            System.setProperty("javax.net.ssl.trustStorePassword", PROPERTIES.getPassphrase());
            return true;
        }

        return false;
    }

    private boolean createKeyStore(String hostName, int port, File keyStore, char[] passphrase) {
        if (keyStore.isFile()) {
            return true;
        }

        try {
            return KeyStoreCreator.createKeyStore(hostName, port, keyStore, passphrase);
        } catch (Exception e) {
            LOG.log(LogLevel.ERROR, "Unable to import certificate", e);
        }

        return false;
    }
}
