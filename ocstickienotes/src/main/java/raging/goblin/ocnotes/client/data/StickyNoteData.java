/*
 * Copyright 2015, RagingGoblin <http://raginggoblin.wordpress.com>
 *
 * This file is part of OcNotes.
 *
 *  OcNotess is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OcNotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OcNotes.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.ocnotes.client.data;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.NoArgsConstructor;
import raging.goblin.ocnotes.sync.synchronization.OcNote;

@Data
@NoArgsConstructor
@JsonIgnoreProperties("changeListeners")
public class StickyNoteData {

    private OcNote ocNote;
    private Dimensions dimensions;
    private int colorIndex;
    private boolean moved = false;
    private boolean sizeChanged = false;

    private List<StickyNoteChangeListener> changeListeners = new ArrayList<>();

    public StickyNoteData(OcNote ocNote, Dimensions dimensions) {
        this.ocNote = ocNote;
        this.dimensions = dimensions;
    }

    public void addChangeListener(StickyNoteChangeListener listener) {
        changeListeners.add(listener);
    }

    public void removeChangeListener(StickyNoteChangeListener listener) {
        changeListeners.remove(listener);
    }

    public void setOcNote(OcNote ocNote, Object source) {
        this.ocNote = ocNote;
        notifyChange(source);
    }

    public void setDimensions(Dimensions dimensions, Object source) {
        this.dimensions = dimensions;
        notifyChange(source);
    }

    public void delete(Object source) {
        new ArrayList<>(changeListeners).forEach(listener -> listener.stickyNoteIsDeleted(this, source));
    }

    private void notifyChange(Object source) {
        changeListeners.forEach(listener -> listener.stickyNoteHasChanged(source));
    }
}
