/*
 * Copyright 2015, RagingGoblin <http://raginggoblin.wordpress.com>
 *
 * This file is part of OcNotes.
 *
 *  OcNotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OcNotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OcNotes.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.ocnotes.client.data;

import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import raging.goblin.ocnotes.client.LogForJLogger;
import raging.goblin.ocnotes.client.gui.StickyNote;
import raging.goblin.ocnotes.client.properties.OcNotesProperties;
import raging.goblin.ocnotes.sync.log.LogLevel;
import raging.goblin.ocnotes.sync.log.Logger;
import raging.goblin.ocnotes.sync.synchronization.OcNote;
import raging.goblin.ocnotes.sync.synchronization.OcNotesStorage;

/**
 * This {@link OcNotesStorage} keeps track of all {@link StickyNote}s and saves its data to local disk.
 */
public class StickyNotesStorage implements OcNotesStorage, StickyNoteChangeListener {

    private static final String DATA_FILE = "notes.json";
    private static final Logger LOG = new LogForJLogger(StickyNotesStorage.class);
    private static final OcNotesProperties PROPERTIES = OcNotesProperties.getInstance();

    private ObjectMapper mapper = new ObjectMapper();
    private Map<Integer, StickyNote> stickyNotes = new HashMap<>();
    private int minInfologId = -1;
    private boolean notesVisible = false;
    private List<StorageChangeListener> storageChangeListeners = new ArrayList<>();

    public StickyNotesStorage() {
        initStickyNotes();
    }

    @Override
    public List<OcNote> updateOcNotes(List<OcNote> ocNotes, String username, String password) {

        ocNotes.forEach(ocNote -> getStickyNoteById(ocNote.getId())
                .ifPresent(sn -> sn.getData().setOcNote(ocNote, this)));

        writeToDisk();
        notifyChangeListeners();
        return getOcNotes(username, password);
    }

    @Override
    public List<OcNote> addOcNotes(List<OcNote> ocNotes, String username, String password) {

        ocNotes.forEach(ocNote -> {
            StickyNoteData noteData = createNoteData(ocNote);
            noteData.addChangeListener(this);
            stickyNotes.put(ocNote.getId(), new StickyNote(noteData, notesVisible));
        });

        writeToDisk();
        notifyChangeListeners();
        return getOcNotes(username, password);
    }

    @Override
    public void removeOcNotes(List<OcNote> ocNotes, String username, String password) {

        ocNotes.forEach(ocNote -> {
            Optional<StickyNote> stickyNote = getStickyNoteById(ocNote.getId());
            stickyNote.ifPresent(sn -> {
                sn.getData().removeChangeListener(this);
                sn.getData().delete(this);
                stickyNotes.remove(ocNote.getId());
            });
        });

        notifyChangeListeners();
        writeToDisk();
    }

    @Override
    public List<OcNote> getOcNotes(String username, String password) {

        return stickyNotes.values().stream()
                .map(stickyNote -> stickyNote.getData().getOcNote())
                .collect(Collectors.toList());
    }

    @Override
    public void updateId(int oldId, int newId) {

        StickyNote toUpdate = stickyNotes.remove(oldId);
        if (toUpdate != null) {
            OcNote oldOcNote = toUpdate.getData().getOcNote();
            OcNote newOcNote = new OcNote(newId, oldOcNote.getModified(), oldOcNote.getContent());
            toUpdate.getData().setOcNote(newOcNote, this);
            stickyNotes.put(newId, toUpdate);
            notifyChangeListeners();
            writeToDisk();
        }
    }

    @Override
    public Optional<OcNote> findOcNoteByContent(final String content) {
        return stickyNotes.values().stream()
                .map(stickyNote -> stickyNote.getData().getOcNote())
                .filter(ocNote -> ocNote.getContent().equals(content))
                .findFirst();
    }

    @Override
    public void stickyNoteHasChanged(Object source) {
        if (source != this) {
            notifyChangeListeners();
            writeToDisk();
        }
    }

    @Override
    public void stickyNoteIsDeleted(StickyNoteData deleted, Object source) {
        if (source != this) {
            deleted.removeChangeListener(this);
            stickyNotes.remove(deleted.getOcNote().getId());
            notifyChangeListeners();
            writeToDisk();
        }
    }

    public void addStorageChangeListener(StorageChangeListener listener) {
        this.storageChangeListeners.add(listener);
    }

    public List<StickyNote> getStickyNotes() {
        return new ArrayList<>(stickyNotes.values());
    }

    public void createStickyNote() {
        OcNote ocNote = new OcNote(--minInfologId);
        StickyNoteData noteData = createNoteData(ocNote);
        noteData.addChangeListener(this);
        stickyNotes.put(ocNote.getId(), new StickyNote(noteData, true));
        notifyChangeListeners();
        writeToDisk();
    }

    public boolean getNotesVisible() {
        return notesVisible;
    }

    public void setNotesVisible(boolean notesVisible) {
        this.notesVisible = notesVisible;
    }

    @SuppressWarnings("static-access")
    private StickyNoteData createNoteData(OcNote ocNote) {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension resolution = toolkit.getScreenSize();
        int maxX = PROPERTIES.SCREEN_BORDER_SPACING;
        int maxY = PROPERTIES.SCREEN_BORDER_SPACING;

        Optional<StickyNote> furthestOnScreen = stickyNotes.values().stream().filter(stickyNote -> !stickyNote.hasMoved())
                .sorted((sn1, sn2) -> new Integer(sn2.getY()).compareTo(new Integer(sn1.getY())))
                .sorted((sn1, sn2) -> new Integer(sn2.getX()).compareTo(new Integer(sn1.getX()))).findFirst();

        if (furthestOnScreen.isPresent()) {
            maxX = furthestOnScreen.get().getX();
            maxY = furthestOnScreen.get().getY() + PROPERTIES.getSpacing();

            if (maxY + PROPERTIES.getHeight() > resolution.getHeight() - PROPERTIES.SCREEN_BORDER_SPACING) {
                maxY = PROPERTIES.SCREEN_BORDER_SPACING;
                maxX += PROPERTIES.getWidth() + PROPERTIES.HORIZONTAL_SPACING;
                if (maxX + PROPERTIES.getWidth() > resolution.getWidth() - PROPERTIES.SCREEN_BORDER_SPACING) {
                    maxX = PROPERTIES.SCREEN_BORDER_SPACING;
                    maxY = PROPERTIES.SCREEN_BORDER_SPACING;
                }
            }
        }

        return new StickyNoteData(ocNote, new Dimensions(maxX, maxY, PROPERTIES.getWidth(), PROPERTIES.getHeight()));
    }

    private synchronized void writeToDisk() {
        try {
            mapper.writeValue(new File(DATA_FILE), getDataObjects());
        } catch (IOException e) {
            LOG.log(LogLevel.ERROR, "Unable to marshall notes", e);
        }
    }

    private List<StickyNoteData> getDataObjects() {
        return stickyNotes.values().stream().map(stickyNote -> stickyNote.getData()).collect(Collectors.toList());
    }

    private void initStickyNotes() {
        for (StickyNoteData noteData : readFromDisk()) {
            if (noteData.getOcNote().getId() < minInfologId) {
                minInfologId = noteData.getOcNote().getId();
            }

            noteData.addChangeListener(this);
            stickyNotes.put(noteData.getOcNote().getId(), new StickyNote(noteData, false));
        }
    }

    private List<StickyNoteData> readFromDisk() {
        File jsonFile = new File(DATA_FILE);
        if (!jsonFile.exists()) {
            try (PrintWriter writer = new PrintWriter(new FileWriter(jsonFile))) {
                writer.println("[]");
            } catch (IOException e) {
                LOG.log(LogLevel.ERROR, "Unable to create notes file", e);
            }
        }

        try {
            return mapper.readValue(jsonFile, new TypeReference<ArrayList<StickyNoteData>>() {
            });
        } catch (IOException e) {
            LOG.log(LogLevel.ERROR, "Unable to read notes file", e);
        }
        System.exit(1);
        return null;
    }

    private Optional<StickyNote> getStickyNoteById(int id) {
        return Optional.ofNullable(stickyNotes.get(id));
    }

    private void notifyChangeListeners() {
        storageChangeListeners.forEach(listener -> listener.storageHasChanged());
    }
}
