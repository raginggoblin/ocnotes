/*
 * Copyright 2015, RagingGoblin <http://raginggoblin.wordpress.com>
 *
 * This file is part of OcNotes.
 *
 *  OcNotess is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OcNotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OcNotes.  If not, see <http://www.gnu.org/licenses/>.
 */
package raging.goblin.ocnotes.client.gui;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

import javax.swing.*;

import dorkbox.systemTray.Menu;
import dorkbox.systemTray.MenuItem;
import dorkbox.systemTray.SystemTray;
import raging.goblin.ocnotes.client.LogForJLogger;
import raging.goblin.ocnotes.client.Messages;
import raging.goblin.ocnotes.client.SynchronizationHandler;
import raging.goblin.ocnotes.client.data.Dimensions;
import raging.goblin.ocnotes.client.data.StickyNotesStorage;
import raging.goblin.ocnotes.client.data.StorageChangeListener;
import raging.goblin.ocnotes.client.properties.OcNotesProperties;
import raging.goblin.ocnotes.sync.log.LogLevel;
import raging.goblin.ocnotes.sync.log.Logger;
import raging.goblin.swingutils.AboutWindow;
import raging.goblin.swingutils.ScreenPositioner;


/**
 * The {@link ClientGui} is a window to login and to show, hide, delete and create {@link StickyNote}s.
 */
public class ClientGui extends JFrame implements StorageChangeListener {

    private static final OcNotesProperties PROPERTIES = OcNotesProperties.getInstance();
    private static final Messages MESSAGES = Messages.getInstance();
    private static final Logger LOG = new LogForJLogger(ClientGui.class);

    private static final String CLIENT_TITLE = MESSAGES.get("client_title");
    private static final String SYNCHRONIZATION_ERROR_TITLE = MESSAGES.get("synchronization_error_title");
    private static final String SYNCHRONIZATION_ERROR = MESSAGES.get("synchronization_error");

    private static final int ICON_GAP = 10;
    private static final int EXTRA_SIZE = 80;

    private static final String LOGO_ICON = "/icons/notes_black.png";

    private static final String EXIT_ICON = "/icons/cross.png";
    private static final String NOTE_ICON = "/icons/note_add.png";
    private static final String SHOW_GUI_ICON = "/icons/application_form_magnify.png";
    private static final String VISIBILITY_ICON = "/icons/application_view_tile.png";
    private static final String HIDE_ICON = "/icons/application.png";
    private static final String LIST_NOTES_ICON = "/icons/text_list_bullets.png";

    private static final ImageIcon ABOUT_ICON = getIcon("/icons/star.png");
    private static final ImageIcon SYNC_CONFIG_ICON = getIcon("/icons/database_edit.png");
    private static final ImageIcon GUI_CONFIG_ICON = getIcon("/icons/wrench.png");
    private static final ImageIcon LAYOUT_ICON = getIcon("/icons/application_cascade.png");
    private static final ImageIcon RESET_SIZE_ICON = getIcon("/icons/arrow_in.png");
    private static final ImageIcon RESET_SYNCHRONIZATION_ICON = getIcon("/icons/arrow_rotate_clockwise.png");
    private static final ImageIcon SYNC_ICON = getIcon("/icons/database_refresh.png");
    private static final ImageIcon HELP_ICON = getIcon("/icons/help.png");
    private static final ImageIcon RESET_FROM_SERVER_ICON = getIcon("/icons/arrow_down.png");
    private static final ImageIcon RESET_FROM_CLIENT_ICON = getIcon("/icons/arrow_up.png");

    private SystemTray systemTray;
    private Menu mainMenu;
    private Menu notesMenu;

    private double maxPreferredWidth;
    private StickyNotesStorage storage;
    private SynchronizationHandler synchronizationHandler;

    private SyncConfigDialog syncConfigDialog;
    private GuiConfigDialog guiConfigDialog;
    private HelpDialog helpDialog;
    private AboutWindow aboutWindow;

    private ActionListener resetSizeListener = e -> {
        forEachStickyNote(sn -> {
            int x = sn.getData().getDimensions().getX();
            int y = sn.getData().getDimensions().getY();
            Dimensions newDimensions = new Dimensions(x, y, PROPERTIES.getWidth(), PROPERTIES.getHeight());
            sn.getData().setDimensions(newDimensions, ClientGui.this);
            sn.getData().setSizeChanged(false);
        });
    };

    private ActionListener resetSynchronizationListener = e -> {
        int response = JOptionPane.showConfirmDialog(this, MESSAGES.get("confirm_reset_synchronization"),
                MESSAGES.get("reset_synchronization"), JOptionPane.OK_CANCEL_OPTION);
        if (response == JOptionPane.OK_OPTION) {
            run(() -> synchronizationHandler.resetSynchronization());
        }
        File keyStore = new File(PROPERTIES.getKeystore());
        if (keyStore.exists()) {
            keyStore.deleteOnExit();
        }
        System.exit(0);
    };

    private ActionListener resetFromServerHandler = e -> {
        run(() -> {
            try {
                synchronizationHandler.resetFromServer();
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(ClientGui.this, SYNCHRONIZATION_ERROR + ex, SYNCHRONIZATION_ERROR_TITLE,
                        JOptionPane.INFORMATION_MESSAGE);
                LOG.log(LogLevel.ERROR, "Unable to Synchronize", ex);
            }
        });
    };

    private ActionListener resetFromClientHandler = e -> {
        run(() -> {
            try {
                synchronizationHandler.resetFromClient();
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(ClientGui.this, SYNCHRONIZATION_ERROR + ex, SYNCHRONIZATION_ERROR_TITLE,
                        JOptionPane.INFORMATION_MESSAGE);
                LOG.log(LogLevel.ERROR, "Unable to Synchronize", ex);
            }
        });
    };

    private ActionListener syncListener = e -> {
        run(() -> {
            showNotes();
            try {
                synchronizationHandler.synchronize(true);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(ClientGui.this, SYNCHRONIZATION_ERROR + ex, SYNCHRONIZATION_ERROR_TITLE,
                        JOptionPane.INFORMATION_MESSAGE);
                LOG.log(LogLevel.ERROR, "Unable to Synchronize", ex);
            }
        });
    };

    private ActionListener toggleListener = e -> {
        storage.setNotesVisible(!storage.getNotesVisible());
        forEachStickyNote(sn -> sn.setVisible(storage.getNotesVisible()));
    };

    private ActionListener showListener = e -> {
        storage.setNotesVisible(true);
        forEachStickyNote(sn -> sn.setVisible(true));
    };

    private ActionListener hideListener = e -> {
        storage.setNotesVisible(false);
        forEachStickyNote(sn -> sn.setVisible(false));
    };

    private ActionListener newNoteListener = e -> {
        showNotes();
        storage.createStickyNote();
    };

    private ActionListener quitListener = e -> {
        System.exit(NORMAL);
    };

    @SuppressWarnings("static-access")
    private ActionListener layoutListener = e -> {
        run(() -> {
            int x = PROPERTIES.SCREEN_BORDER_SPACING;
            int y = PROPERTIES.SCREEN_BORDER_SPACING;

            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Dimension resolution = toolkit.getScreenSize();

            for (StickyNote stickyNote : storage.getStickyNotes()) {
                int width = stickyNote.getData().getDimensions().getWidth();
                int height = stickyNote.getData().getDimensions().getHeight();
                Dimensions dimensions = new Dimensions(x, y, width, height);
                stickyNote.getData().setDimensions(dimensions, ClientGui.this);
                stickyNote.getData().setMoved(false);
                y += PROPERTIES.getSpacing();
                if (y + PROPERTIES.getHeight() > resolution.getHeight() - PROPERTIES.SCREEN_BORDER_SPACING) {
                    y = PROPERTIES.SCREEN_BORDER_SPACING;
                    x += PROPERTIES.getWidth() + PROPERTIES.HORIZONTAL_SPACING;
                    if (x + PROPERTIES.getWidth() > resolution.getWidth() - PROPERTIES.SCREEN_BORDER_SPACING) {
                        x = PROPERTIES.SCREEN_BORDER_SPACING;
                        y = PROPERTIES.SCREEN_BORDER_SPACING;
                    }
                }
            }
        });
    };

    public ClientGui(StickyNotesStorage storage, SynchronizationHandler synchronizationHandler) {
        super(CLIENT_TITLE);
        this.storage = storage;
        storage.addStorageChangeListener(this);
        this.synchronizationHandler = synchronizationHandler;
        loadSystray();
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(LOGO_ICON)));
        initGui();
        setDefaultCloseOperation(systemTray != null ? HIDE_ON_CLOSE : EXIT_ON_CLOSE);
    }

    @Override
    public void storageHasChanged() {
        updateNotesMenu();
    }

    private static Font loadIconFont(String resource) {
        InputStream is = ClientGui.class.getResourceAsStream(resource);
        try {
            return Font.createFont(Font.TRUETYPE_FONT, is);
        } catch (FontFormatException | IOException e) {
            LOG.log(LogLevel.ERROR, "Unable to load font", e);
        }
        return new Font(PROPERTIES.getTitleFontFamily(), PROPERTIES.getTitleFontStyle(), PROPERTIES.getTitleFontSize());
    }

    private void initGui() {
        add(createActionsPanel(), BorderLayout.CENTER);
        add(new JPanel(), BorderLayout.EAST);
        add(new JPanel(), BorderLayout.WEST);
        add(new JPanel(), BorderLayout.SOUTH);
        add(new JPanel(), BorderLayout.NORTH);
        setJMenuBar(createMenu());
        setSize((int) maxPreferredWidth + EXTRA_SIZE, 220);
        ScreenPositioner.centerOnScreen(this);
    }

    private void loadSystray() {
//        SystemTray.APP_NAME = MESSAGES.get("client_title");
        systemTray = SystemTray.get();
        if (systemTray == null) {
            LOG.log(LogLevel.WARN, "TrayIcon could not be added.");
            setVisible(true);
            return;
        }

        systemTray.setImage(Paths.get(PROPERTIES.getTrayIcon()).toFile());
        mainMenu = systemTray.getMenu();
        notesMenu = new Menu(MESSAGES.get("notes"), getClass().getResource(LIST_NOTES_ICON));

        mainMenu.add(new dorkbox.systemTray.MenuItem(MESSAGES.get("show"), getClass().getResource(VISIBILITY_ICON), showListener));
        mainMenu.add(new dorkbox.systemTray.MenuItem(MESSAGES.get("hide"), getClass().getResource(HIDE_ICON), hideListener));
        mainMenu.add(new dorkbox.systemTray.MenuItem(MESSAGES.get("show_gui"), getClass().getResource(SHOW_GUI_ICON), e -> setVisible(true)));
        mainMenu.add(new dorkbox.systemTray.MenuItem(MESSAGES.get("new_note"), getClass().getResource(NOTE_ICON), newNoteListener));
        mainMenu.add(new dorkbox.systemTray.MenuItem(MESSAGES.get("quit"), getClass().getResource(EXIT_ICON), quitListener));

        updateNotesMenu();
    }

    private void updateNotesMenu() {
        SwingUtilities.invokeLater(() -> {

            mainMenu.remove(notesMenu);
            notesMenu.remove();

            final Comparator<StickyNote> stickyNoteComparator = (StickyNote o1, final StickyNote o2) ->
                    o1.getData().getOcNote().getTitle().compareTo(o2.getData().getOcNote().getTitle());
            final Map<Integer, Image> icons = createNotesMenuIcons();

            try {
                List<StickyNote> stickyNotes = storage.getStickyNotes();
                stickyNotes.stream().sorted(stickyNoteComparator).forEach(stickyNote -> {
                    ActionListener action = e -> {
                        stickyNote.setVisible(true);
                        stickyNote.requestFocus();
                    };
                    MenuItem menuItem = new MenuItem(stickyNote.getData().getOcNote().getTitle(), icons.get(stickyNote.getData().getColorIndex()), action);
                    notesMenu.add(menuItem);
                });
            } catch (Exception e) {
                LOG.log(LogLevel.ERROR, "Cannot create systray notes menu", e);
            }
            mainMenu.add(notesMenu, 0);
        });
    }

    private Map<Integer, Image> createNotesMenuIcons() {
        Map<Integer, Image> icons = new HashMap<>();
        for (int i = 0; i < PROPERTIES.getNoteTopColors().length; i++) {
            JPanel panel = new JPanel();
            panel.setOpaque(true);
            panel.setSize(10, 10);
            panel.setBackground(new Color(PROPERTIES.getNoteTopColors()[i]));
            BufferedImage image = new BufferedImage(10, 10, BufferedImage.TYPE_INT_ARGB);
            panel.paint(image.getGraphics());
            icons.put(i, image);
        }
        return icons;
    }

    private JMenuBar createMenu() {
        JMenuBar menuBar = new JMenuBar();

        JMenu fileMenu = new JMenu(MESSAGES.get("file"));

        JMenuItem newNoteMenuItem = new JMenuItem(MESSAGES.get("new_note"), getIcon(NOTE_ICON));
        newNoteMenuItem.addActionListener(newNoteListener);
        fileMenu.add(newNoteMenuItem);

        JMenuItem quitItem = new JMenuItem(MESSAGES.get("quit"), getIcon(EXIT_ICON));
        quitItem.addActionListener(quitListener);
        fileMenu.add(quitItem);

        menuBar.add(fileMenu);

        JMenu actionsMenu = new JMenu(MESSAGES.get("actions"));

        JMenuItem configureSyncItem = new JMenuItem(MESSAGES.get("configure_synchronization"), SYNC_CONFIG_ICON);
        configureSyncItem.addActionListener(e -> {
            synchronizationHandler.stop();
            showSyncConfigDialog();
            if (PROPERTIES.isSynchronizationEnabled()) {
                synchronizationHandler.start();
            }
        });
        actionsMenu.add(configureSyncItem);

        JMenuItem configureGuiItem = new JMenuItem(MESSAGES.get("configure_look_and_feel"), GUI_CONFIG_ICON);
        configureGuiItem.addActionListener(e -> {
            showGuiConfigDialog();
        });
        actionsMenu.add(configureGuiItem);

        actionsMenu.addSeparator();

        JMenuItem syncItem = new JMenuItem(MESSAGES.get("synchronize"), SYNC_ICON);
        syncItem.addActionListener(syncListener);
        actionsMenu.add(syncItem);

        JMenuItem resetSyncItem = new JMenuItem(MESSAGES.get("reset_synchronization"), RESET_SYNCHRONIZATION_ICON);
        resetSyncItem.addActionListener(resetSynchronizationListener);
        actionsMenu.add(resetSyncItem);

        JMenuItem resetFromServerItem = new JMenuItem(MESSAGES.get("reset_from_server"), RESET_FROM_SERVER_ICON);
        resetFromServerItem.addActionListener(resetFromServerHandler);
        actionsMenu.add(resetFromServerItem);

        JMenuItem resetFromClientItem = new JMenuItem(MESSAGES.get("reset_from_client"), RESET_FROM_CLIENT_ICON);
        resetFromClientItem.addActionListener(resetFromClientHandler);
        actionsMenu.add(resetFromClientItem);

        actionsMenu.addSeparator();

        JMenuItem resetSizeItem = new JMenuItem(MESSAGES.get("reset_size"), RESET_SIZE_ICON);
        resetSizeItem.addActionListener(resetSizeListener);
        actionsMenu.add(resetSizeItem);

        JMenuItem layoutItem = new JMenuItem(MESSAGES.get("layout"), LAYOUT_ICON);
        layoutItem.addActionListener(layoutListener);
        actionsMenu.add(layoutItem);

        JMenuItem toggleItem = new JMenuItem(MESSAGES.get("toggle_visibility"), getIcon(VISIBILITY_ICON));
        toggleItem.addActionListener(toggleListener);
        actionsMenu.add(toggleItem);

        menuBar.add(actionsMenu);

        JMenu helpMenu = new JMenu(MESSAGES.get("help"));

        JMenuItem helpItem = new JMenuItem(MESSAGES.get("help"), HELP_ICON);
        helpItem.addActionListener(e -> {
            showHelpDialog();
        });
        helpMenu.add(helpItem);

        JMenuItem aboutItem = new JMenuItem(MESSAGES.get("about"), ABOUT_ICON);
        aboutItem.addActionListener(e -> showAboutInfo());
        helpMenu.add(aboutItem);

        menuBar.add(helpMenu);

        return menuBar;
    }

    private JPanel createActionsPanel() {
        JPanel actionsPanel = new JPanel(new GridLayout(0, 1, 10, 5));

        JButton newNoteButton = new JButton(MESSAGES.get("new_note"), getIcon(NOTE_ICON));
        newNoteButton.addActionListener(newNoteListener);
        actionsPanel.add(newNoteButton);

        JButton synchronizeButton = new JButton(MESSAGES.get("synchronize"), SYNC_ICON);
        synchronizeButton.setIconTextGap(ICON_GAP);
        synchronizeButton.addActionListener(syncListener);
        actionsPanel.add(synchronizeButton);

        JButton toggleButton = new JButton(MESSAGES.get("toggle_visibility"), getIcon(VISIBILITY_ICON));
        toggleButton.setIconTextGap(ICON_GAP);
        toggleButton.addActionListener(toggleListener);
        actionsPanel.add(toggleButton);

        maxPreferredWidth = toggleButton.getPreferredSize().getWidth();
        return actionsPanel;
    }

    private void showNotes() {
        forEachStickyNote(sn -> sn.setVisible(true));
    }

    private void showHelpDialog() {
        if (helpDialog == null) {
            helpDialog = new HelpDialog(this);
            helpDialog.pack();
        }
        helpDialog.setVisible(true);
    }

    private void showSyncConfigDialog() {
        if (syncConfigDialog == null) {
            syncConfigDialog = new SyncConfigDialog(this, synchronizationHandler);
            syncConfigDialog.pack();
        }
        syncConfigDialog.setVisible(true);
    }

    private void showGuiConfigDialog() {
        if (guiConfigDialog == null) {
            guiConfigDialog = new GuiConfigDialog(this);
            guiConfigDialog.pack();
        }
        guiConfigDialog.setOkPressed(false);
        guiConfigDialog.setVisible(true);
        if (guiConfigDialog.isOkPressed()) {
            resetSizeListener.actionPerformed(null);
            layoutListener.actionPerformed(null);
            forEachStickyNote(sn -> sn.guiConfigChanged());
        }
    }

    private void showAboutInfo() {
        if (aboutWindow == null) {
            aboutWindow = new AboutWindow(this, MESSAGES.get("about_title"), Optional.of("/icons/notes_black.png"),
                    Optional.of(MESSAGES.get("version_text")), Optional.of(MESSAGES.get("url_text")),
                    Optional.of(MESSAGES.get("about_text")), Optional.of(MESSAGES.get("license_text")));
            aboutWindow.pack();
        }
        aboutWindow.setSize(510, 275);
        aboutWindow.setVisible(true);
    }

    private static ImageIcon getIcon(String resource) {
        return new ImageIcon(ClientGui.class.getResource(resource));
    }

    private void forEachStickyNote(Consumer<StickyNote> action) {
        new Thread(() -> storage.getStickyNotes().forEach(action)).run();
    }

    private void run(Runnable runnable) {
        new Thread(runnable).start();
    }
}
