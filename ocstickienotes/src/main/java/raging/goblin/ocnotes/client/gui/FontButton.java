/*
 * Copyright 2015, RagingGoblin <http://raginggoblin.wordpress.com>
 * 
 * This file is part of OcNotes.
 *
 *  OcNotess is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OcNotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OcNotes.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.ocnotes.client.gui;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;

/**
 * A button displaying a font, by clicking on the user will be capable of changing the font.
 */
public class FontButton extends JButton implements MouseListener {

	private static final String[] FONT_STYLES = { "Plain", "Bold", "Italic", "Bold/Italic" };

	private String fontFamily;
	private int fontStyle;
	private int fontSize;
	private JFontChooser chooser;

	public FontButton(String fontFamily, int fontStyle, int fontSize) {
		this.fontFamily = fontFamily;
		this.fontStyle = fontStyle;
		this.fontSize = fontSize;
		chooser = new JFontChooser();
		chooser.setSelectedFontFamily(fontFamily);
		chooser.setSelectedFontStyle(fontStyle);
		chooser.setSelectedFontSize(fontSize);
		setText(getFontName());
		addMouseListener(this);
	}

	public String getFontFamily() {
		return fontFamily;
	}

	public int getFontStyle() {
		return fontStyle;
	}

	public int getFontSize() {
		return fontSize;
	}

	public void setFont(String fontFamily, int fontStyle, int fontSize) {
		this.fontFamily = fontFamily;
		this.fontStyle = fontStyle;
		this.fontSize = fontSize;
		chooser.setSelectedFontFamily(fontFamily);
		chooser.setSelectedFontStyle(fontStyle);
		chooser.setSelectedFontSize(fontSize);
		setText(getFontName());
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// Nothing to do
	}

	@Override
	public void mousePressed(MouseEvent e) {
		requestFocusInWindow();
		int result = chooser.showDialog(this);
		if (result == JFontChooser.OK_OPTION) {
			fontFamily = chooser.getSelectedFontFamily();
			fontStyle = chooser.getSelectedFontStyle();
			fontSize = chooser.getSelectedFontSize();
			setText(getFontName());
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// Nothing to do
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// Nothing to do
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// Nothing to do
	}

	private String getFontName() {
		return fontFamily + ", " + FONT_STYLES[fontStyle] + ", " + fontSize;
	}
}
