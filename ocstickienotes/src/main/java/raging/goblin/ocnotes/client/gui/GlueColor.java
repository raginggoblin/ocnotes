/*
 * Copyright 2015, RagingGoblin <http://raginggoblin.wordpress.com>
 * 
 * This file is part of OcNotes.
 *
 *  OcNotess is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OcNotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OcNotes.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.ocnotes.client.gui;

import java.awt.*;

public final class GlueColor {

	private static final int GLUE_DISCOUNT = 20;

	private GlueColor() {
		// Utility class
	}

	public static int getGlueColor(int topColor) {
		Color top = new Color(topColor);
		int r = Math.max(0, top.getRed() - GLUE_DISCOUNT);
		int g = Math.max(0, top.getGreen() - GLUE_DISCOUNT);
		int b = Math.max(0, top.getBlue() - GLUE_DISCOUNT);
		return new Color(r, g, b).getRGB();
	}
}
