/*
 * Copyright 2015, RagingGoblin <http://raginggoblin.wordpress.com>
 *
 * This file is part of OcNotes.
 *
 *  OcNotess is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OcNotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OcNotes.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.ocnotes.client.gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.*;

import com.jgoodies.forms.debug.FormDebugPanel;
import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import lombok.Getter;
import lombok.Setter;
import raging.goblin.ocnotes.client.Messages;
import raging.goblin.ocnotes.client.properties.OcNotesProperties;
import raging.goblin.swingutils.ColorButton;
import raging.goblin.swingutils.ScreenPositioner;
import raging.goblin.swingutils.StringSeparator;

/**
 * A dialog to configure the look and feel of the gui and notes.
 */
public class GuiConfigDialog extends JDialog {

    private enum Language {
        English, Nederlands
    }

    private static final OcNotesProperties PROPERTIES = OcNotesProperties.getInstance();
    private static final Messages MESSAGES = Messages.getInstance();

    private JComboBox<Language> languageBox;
    private JSpinner widthSpinner;
    private JSpinner spacingSpinner;
    private JSpinner heightSpinner;
    private List<ColorButton> btnTopColorNotes = new ArrayList<>();
    private List<ColorButton> btnBottomColorNotes = new ArrayList<>();
    private ColorButton btnTitleColorNotes;
    private ColorButton btnContentColorNotes;
    private FontButton btnTitleFont;
    private FontButton btnContentFont;
    private JSpinner dragDelaySpinner;
    private JSpinner doubleClickDelaySpinner;
    private JTextField trayIconField;
    private JTextField dateTimeFormatField;

    @Getter
    @Setter
    private boolean okPressed;

    public GuiConfigDialog(JFrame parent) {
        super(parent, MESSAGES.get("config_title"), true);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        initSize(1150, 825);
        ScreenPositioner.centerOnScreen(this);

        JPanel actionPanel = new JPanel();
        getContentPane().add(actionPanel, BorderLayout.SOUTH);
        FormLayout layout = new FormLayout(
                new ColumnSpec[]{ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC,
                        FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
                        FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("max(40dlu;default)"),
                        FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
                        ColumnSpec.decode("max(40dlu;default)"), FormFactory.RELATED_GAP_COLSPEC,
                        ColumnSpec.decode("max(5dlu;default)"),},
                new RowSpec[]{FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,});
        actionPanel.setLayout(layout);

        JButton btnCancel = new JButton(MESSAGES.get("cancel"));
        btnCancel.addActionListener(e -> setVisible(false));
        actionPanel.add(btnCancel, "7, 2, 2, 1");

        JButton btnDefaults = new JButton(MESSAGES.get("load_defaults"));
        btnDefaults.addActionListener(e -> setDefaults());
        actionPanel.add(btnDefaults, "3, 2");

        JButton btnOk = new JButton(MESSAGES.get("ok"));
        btnOk.addActionListener(e -> {
            okPressed = true;
            saveConfiguration();
            setVisible(false);
        });
        actionPanel.add(btnOk, "11, 2, 2, 1");

        JPanel configPanel = new JPanel();
        JScrollPane scrollPane = new JScrollPane(configPanel);
        getContentPane().add(scrollPane, BorderLayout.CENTER);
        configPanel.setLayout(new FormLayout(new ColumnSpec[]{

                ColumnSpec.decode("max(61dlu;min):grow"), ColumnSpec.decode("max(40dlu;default):grow"),
                FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("99px:grow"), FormFactory.RELATED_GAP_COLSPEC,

                ColumnSpec.decode("max(30dlu;default)"),

                FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
                FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"),

                FormFactory.RELATED_GAP_COLSPEC,

                ColumnSpec.decode("max(30dlu;default)"), FormFactory.RELATED_GAP_COLSPEC,
                ColumnSpec.decode("max(30dlu;default)"),},

                new RowSpec[]{FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
                        FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                        FormFactory.DEFAULT_ROWSPEC
                }));

        StringSeparator stringSeparator = new StringSeparator(MESSAGES.get("language"));
        configPanel.add(stringSeparator, "1, 2, 18, 1");

        languageBox = new JComboBox<>(Language.values());
        languageBox.setSelectedItem(getLanguage(PROPERTIES.getLanguage()));
        languageBox.setToolTipText(MESSAGES.get("language_tooltip"));
        configPanel.add(languageBox, "16, 4, fill, default");

        StringSeparator dimensionsSeparator = new StringSeparator(MESSAGES.get("dimensions"));
        configPanel.add(dimensionsSeparator, "1, 6, 18, 1");

        JLabel lblWidth = new JLabel(MESSAGES.get("width"));
        configPanel.add(lblWidth, "4, 8, right, center");

        widthSpinner = new JSpinner();
        widthSpinner.setToolTipText(MESSAGES.get("width_tooltip"));
        configPanel.add(widthSpinner, "6, 8");

        JLabel lblPx_1 = new JLabel("px");
        configPanel.add(lblPx_1, "8, 8");

        JLabel lblHeight = new JLabel(MESSAGES.get("height"));
        configPanel.add(lblHeight, "12, 8, 3, 1, right, center");

        heightSpinner = new JSpinner();
        heightSpinner.setToolTipText(MESSAGES.get("height_tooltip"));
        configPanel.add(heightSpinner, "16, 8");

        JLabel lblPx = new JLabel("px");
        configPanel.add(lblPx, "18, 8");

        JLabel lblSpacing = new JLabel(MESSAGES.get("spacing"));
        configPanel.add(lblSpacing, "4, 9, right, center");

        spacingSpinner = new JSpinner();
        spacingSpinner.setToolTipText(MESSAGES.get("spacing_tooltip"));
        configPanel.add(spacingSpinner, "6, 9");

        JLabel lblPx_2 = new JLabel("px");
        configPanel.add(lblPx_2, "8, 9");

        StringSeparator dateTimeFormatSeparator = new StringSeparator(MESSAGES.get("date_time_format"));
        configPanel.add(dateTimeFormatSeparator, "1, 12, 18, 1");

        dateTimeFormatField = new JTextField();
        dateTimeFormatField.setHorizontalAlignment(SwingConstants.RIGHT);
        dateTimeFormatField.setToolTipText(MESSAGES.get("date_time_format_tooltip"));
        configPanel.add(dateTimeFormatField, "12, 14, 5, 1, fill, default");
        dateTimeFormatField.setColumns(25);

        StringSeparator colorsSeparator = new StringSeparator(MESSAGES.get("colors"));
        configPanel.add(colorsSeparator, "1, 16, 18, 1");

        JLabel lblTop = new JLabel(MESSAGES.get("top"));
        configPanel.add(lblTop, "6, 18, center, center");

        JLabel lblBottom = new JLabel(MESSAGES.get("bottom"));
        configPanel.add(lblBottom, "16, 18, center, center");

        for (int i = 0; i < PROPERTIES.getNoteTopColors().length; i++) {
            for (int j = 0; j < 2; j++) {
                JLabel lblIndex = new JLabel("" + (i + 1));
                configPanel.add(lblIndex, (4 + j * 10) + ", " + (20 + i * 2) + ", right, center");
            }

            ColorButton topColorButton = new ColorButton(PROPERTIES.getNoteTopColors()[i]);
            btnTopColorNotes.add(topColorButton);
            topColorButton.setToolTipText(MESSAGES.get("note_top_tooltip"));
            configPanel.add(topColorButton, "6, " + (20 + i * 2) + ", fill, fill");

            ColorButton bottomColorButton = new ColorButton(PROPERTIES.getNoteBottomColors()[i]);
            btnBottomColorNotes.add(bottomColorButton);
            bottomColorButton.setToolTipText(MESSAGES.get("note_bottom_tooltip"));
            configPanel.add(bottomColorButton, "16, " + (20 + i * 2) + ", fill, fill");
        }

        StringSeparator fontsSeparator = new StringSeparator(MESSAGES.get("fonts"));
        configPanel.add(fontsSeparator, "1, 32, 18, 1");

        JLabel lblTitle = new JLabel(MESSAGES.get("title"));
        configPanel.add(lblTitle, "4, 34, right, center");

        btnTitleColorNotes = new ColorButton(PROPERTIES.getNoteTitleColor());
        btnTitleColorNotes.setToolTipText(MESSAGES.get("note_title_color_tooltip"));
        configPanel.add(btnTitleColorNotes, "6, 34, fill, fill");

        JLabel lblContent = new JLabel(MESSAGES.get("content"));
        configPanel.add(lblContent, "12, 34, 3, 1, right, center");

        btnContentColorNotes = new ColorButton(PROPERTIES.getNoteContentColor());
        btnContentColorNotes.setToolTipText(MESSAGES.get("note_content_color_tooltip"));
        configPanel.add(btnContentColorNotes, "16, 34, fill, fill");

        btnTitleFont = new FontButton(PROPERTIES.getTitleFontFamily(), PROPERTIES.getTitleFontStyle(),
                PROPERTIES.getTitleFontSize());
        btnTitleFont.setToolTipText(MESSAGES.get("note_title_font_tooltip"));
        configPanel.add(btnTitleFont, "6, 36, fill, fill");

        btnContentFont = new FontButton(PROPERTIES.getContentFontFamily(), PROPERTIES.getContentFontStyle(),
                PROPERTIES.getContentFontSize());
        btnContentFont.setToolTipText(MESSAGES.get("note_content_font_tooltip"));
        configPanel.add(btnContentFont, "16, 36, fill, fill");

        StringSeparator behaviourSeparator = new StringSeparator(MESSAGES.get("mouse_behaviour"));
        configPanel.add(behaviourSeparator, "1, 38, 18, 1, fill, default");

        JLabel lblDragDelay = new JLabel(MESSAGES.get("drag_delay"));
        configPanel.add(lblDragDelay, "4, 40, right, center");

        dragDelaySpinner = new JSpinner();
        dragDelaySpinner.setToolTipText(MESSAGES.get("drag_delay_tooltip"));
        configPanel.add(dragDelaySpinner, "6, 40");

        JLabel lblDoubleClickDelay = new JLabel(MESSAGES.get("double_click_delay"));
        configPanel.add(lblDoubleClickDelay, "10, 40, 5, 1, right, center");

        doubleClickDelaySpinner = new JSpinner();
        doubleClickDelaySpinner.setToolTipText(MESSAGES.get("double_click_delay_tooltip"));
        configPanel.add(doubleClickDelaySpinner, "16, 40");

        StringSeparator iconSeparator = new StringSeparator(MESSAGES.get("system_tray_icon"));
        configPanel.add(iconSeparator, "1, 46, 18, 1");

        JLabel lblTrayIcon = new JLabel(MESSAGES.get("tray_icon_file"));
        configPanel.add(lblTrayIcon, "2, 50, 1, 1, right, center");

        trayIconField = new JTextField();
        trayIconField.setToolTipText(MESSAGES.get("systray_tooltip"));
        configPanel.add(trayIconField, "4, 50, 11, 1, fill, default");

        JButton btnChooseIcon = new JButton(MESSAGES.get("choose_file"));
        btnChooseIcon.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            int returnVal = fileChooser.showOpenDialog(GuiConfigDialog.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                trayIconField.setText(file.getPath());
            }
        });
        configPanel.add(btnChooseIcon, "16, 50, 1, 1");

        loadConfiguration();
        setFocus(btnCancel);
    }

    private void initSize(int width, int height) {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(width, (int) Math.min(height, screenSize.getHeight() - 200));
    }

    private void loadConfiguration() {
        widthSpinner.setValue(PROPERTIES.getWidth());
        heightSpinner.setValue(PROPERTIES.getHeight());
        spacingSpinner.setValue(PROPERTIES.getSpacing());
        dateTimeFormatField.setText(PROPERTIES.getTimeFormat());
        dragDelaySpinner.setValue(PROPERTIES.getDragDelay());
        doubleClickDelaySpinner.setValue(PROPERTIES.getDoubleClickDelay());
        trayIconField.setText(PROPERTIES.getTrayIcon());
    }

    private void saveConfiguration() {
        new Thread(() -> {
            PROPERTIES.setLanguage(getLanguageCode((Language) languageBox.getSelectedItem()));
            PROPERTIES.setWidth((Integer) widthSpinner.getValue());
            PROPERTIES.setSpacing((Integer) spacingSpinner.getValue());
            PROPERTIES.setHeight((Integer) heightSpinner.getValue());
            PROPERTIES.setTimeFormat(dateTimeFormatField.getText());
            PROPERTIES.setNoteBottomColors(
                    btnBottomColorNotes.stream().map(b -> "" + b.getColorValue()).collect(Collectors.joining(",")));
            PROPERTIES.setNoteTopColors(
                    btnTopColorNotes.stream().map(b -> "" + b.getColorValue()).collect(Collectors.joining(",")));
            PROPERTIES.setNoteTitleColor(btnTitleColorNotes.getColorValue());
            PROPERTIES.setNoteContentColor(btnContentColorNotes.getColorValue());
            PROPERTIES.setTitleFontFamily(btnTitleFont.getFontFamily());
            PROPERTIES.setTitleFontStyle(btnTitleFont.getFontStyle());
            PROPERTIES.setTitleFontSize(btnTitleFont.getFontSize());
            PROPERTIES.setContentFontFamily(btnContentFont.getFontFamily());
            PROPERTIES.setContentFontStyle(btnContentFont.getFontStyle());
            PROPERTIES.setContentFontSize(btnContentFont.getFontSize());
            PROPERTIES.setDragDelay((Integer) dragDelaySpinner.getValue());
            PROPERTIES.setDoubleClickDelay((Integer) doubleClickDelaySpinner.getValue());
            PROPERTIES.setTrayIcon(trayIconField.getText());
        }).start();
    }

    @SuppressWarnings("static-access")
    private void setDefaults() {
        languageBox.setSelectedItem(Language.English);
        widthSpinner.setValue(PROPERTIES.DEFAULT_WIDTH);
        spacingSpinner.setValue(PROPERTIES.DEFAULT_SPACING);
        heightSpinner.setValue(PROPERTIES.DEFAULT_HEIGHT);
        dateTimeFormatField.setText(PROPERTIES.DEFAULT_TIME_FORMAT);
        for (int i = 0; i < btnBottomColorNotes.size(); i++) {
            btnBottomColorNotes.get(i).setColorValue(PROPERTIES.getDefaultNoteBottomColors()[i]);
        }
        for (int i = 0; i < btnTopColorNotes.size(); i++) {
            btnTopColorNotes.get(i).setColorValue(PROPERTIES.getDefaultNoteTopColors()[i]);
        }
        btnTitleColorNotes.setColorValue(PROPERTIES.DEFAULT_NOTE_TITLE_COLOR);
        btnContentColorNotes.setColorValue(PROPERTIES.DEFAULT_NOTE_CONTENT_COLOR);
        btnTitleFont.setFont(PROPERTIES.DEFAULT_TITLE_FONT_FAMILY, PROPERTIES.DEFAULT_TITLE_FONT_STYLE,
                PROPERTIES.DEFAULT_TITLE_FONT_SIZE);
        btnContentFont.setFont(PROPERTIES.DEFAULT_CONTENT_FONT_FAMILY, PROPERTIES.DEFAULT_CONTENT_FONT_STYLE,
                PROPERTIES.DEFAULT_CONTENT_FONT_SIZE);
        dragDelaySpinner.setValue(PROPERTIES.DEFAULT_DRAG_DELAY);
        doubleClickDelaySpinner.setValue(PROPERTIES.DEFAULT_DOUBLE_CLICK_DELAY);
        trayIconField.setText(PROPERTIES.DEFAULT_TRAY_ICON);
    }

    private void setFocus(final JComponent component) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                component.requestFocusInWindow();
            }
        });
    }

    private String getLanguageCode(Language language) {
        switch (language) {
            case Nederlands:
                return "nl";
            case English:
            default:
                return "en";
        }
    }

    private Language getLanguage(String languageCode) {
        switch (languageCode) {
            case "nl":
                return Language.Nederlands;
            case "en":
            default:
                return Language.English;
        }
    }

    public static void main(String[] args) throws ClassNotFoundException, UnsupportedLookAndFeelException,
            InstantiationException, IllegalAccessException {

        UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
        GuiConfigDialog guiConfigDialog = new GuiConfigDialog(null);
        guiConfigDialog.setDefaultCloseOperation(EXIT_ON_CLOSE);
        guiConfigDialog.setVisible(true);
    }
}
