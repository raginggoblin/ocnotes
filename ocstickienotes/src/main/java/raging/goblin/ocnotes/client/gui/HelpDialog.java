/*
 * Copyright 2015, RagingGoblin <http://raginggoblin.wordpress.com>
 * 
 * This file is part of OcNotes.
 *
 *  OcNotess is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OcNotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OcNotes.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.ocnotes.client.gui;

import java.awt.*;

import javax.swing.*;

import raging.goblin.ocnotes.client.Messages;
import raging.goblin.swingutils.ScreenPositioner;

/**
 * Provides a tiny bit of help to the user.
 */
public class HelpDialog extends JDialog {

	private static final Messages MESSAGES = Messages.getInstance();
	private static final String HEADER = MESSAGES.get("help_header");
	private static final String HELP_TITLE = MESSAGES.get("help_title");
	private static final String HELP_TEXT = MESSAGES.get("help_text");

	public HelpDialog(JFrame parent) {
		super(parent, HEADER, true);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		initGui();
		setSize(830, 255);
		ScreenPositioner.centerOnScreen(this);
	}

	private void initGui() {
		JPanel topPanel = new JPanel();
		getContentPane().add(topPanel, BorderLayout.NORTH);
		topPanel.add(new JLabel(HELP_TITLE));

		JPanel centerPanel = new JPanel();
		getContentPane().add(centerPanel, BorderLayout.CENTER);
		centerPanel.add(new JLabel(HELP_TEXT));
	}
}
