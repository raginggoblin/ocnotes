/*
 * Copyright 2015, RagingGoblin <http://raginggoblin.wordpress.com>
 *
 * This file is part of OcNotes.
 *
 *  OcNotess is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OcNotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OcNotes.  If not, see <http://www.gnu.org/licenses/>.
 */
package raging.goblin.ocnotes.client.gui;

import java.awt.*;
import java.lang.reflect.InvocationTargetException;

import javax.swing.*;

import raging.goblin.ocnotes.client.LogForJLogger;
import raging.goblin.ocnotes.sync.log.LogLevel;
import raging.goblin.ocnotes.sync.log.Logger;
import raging.goblin.ocnotes.sync.synchronization.SynchronizationProgressListener;
import raging.goblin.swingutils.ScreenPositioner;

public class ProgressDialog extends JDialog implements SynchronizationProgressListener {

    private static final Logger LOG = new LogForJLogger(ProgressDialog.class);

    private JProgressBar progressBar;

    public ProgressDialog(JFrame parent, String title) {
        super(parent, title);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setModalityType(ModalityType.APPLICATION_MODAL);
        initGui();
        setUndecorated(true);
        setSize(400, 70);
        ScreenPositioner.centerOnScreen(this);
    }

    @Override
    public void setProgress(int percentage) {
        try {
            SwingUtilities.invokeAndWait(() -> progressBar.setValue(percentage));
        } catch (InvocationTargetException | InterruptedException e) {
            LOG.log(LogLevel.ERROR, "Unable to set progress", e);
        }
    }

    private void initGui() {
        progressBar = new JProgressBar();
        progressBar.setStringPainted(true);
        progressBar.setValue(10);
        getContentPane().add(progressBar, BorderLayout.CENTER);

        getContentPane().add(new JLabel("    "), BorderLayout.EAST);
        getContentPane().add(new JLabel("    "), BorderLayout.NORTH);
        getContentPane().add(new JLabel("    "), BorderLayout.WEST);
        getContentPane().add(new JLabel("    "), BorderLayout.SOUTH);
    }
}
