/*
 * Copyright 2015, RagingGoblin <http://raginggoblin.wordpress.com>
 *
 * This file is part of OcNotes.
 *
 *  OcNotess is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OcNotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OcNotes.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.ocnotes.client.gui;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.MouseInputAdapter;
import javax.swing.text.JTextComponent;

import lombok.AllArgsConstructor;
import lombok.Setter;
import raging.goblin.ocnotes.client.LogForJLogger;
import raging.goblin.ocnotes.client.Messages;
import raging.goblin.ocnotes.client.data.Dimensions;
import raging.goblin.ocnotes.client.data.StickyNoteChangeListener;
import raging.goblin.ocnotes.client.data.StickyNoteData;
import raging.goblin.ocnotes.client.properties.OcNotesProperties;
import raging.goblin.ocnotes.sync.log.LogLevel;
import raging.goblin.ocnotes.sync.log.Logger;
import raging.goblin.ocnotes.sync.synchronization.OcNote;
import raging.goblin.swingutils.AntiAliaser;
import raging.goblin.swingutils.ComponentResizer;
import raging.goblin.swingutils.LookAndFeel;
import raging.goblin.swingutils.TransparentScrollPane;

/**
 * Represents a sticky note, these yellow notes on which you can scribble something you want to remember. It appears as
 * a colored note on your screen containing a title bar with the title you define, a date displayed at the bottom and a
 * text box which you can fill with text.
 */
public class StickyNote extends JFrame implements StickyNoteChangeListener {

    private static final OcNotesProperties OCN_PROPERTIES = OcNotesProperties.getInstance();
    private static final Messages MESSAGES = Messages.getInstance();
    private static final Logger LOG = new LogForJLogger(StickyNote.class);

    private static final int BORDERWIDTH = 10;
    private static final ImageIcon CUT_ICON = getIcon("/icons/cut.png");
    private static final ImageIcon COPY_ICON = getIcon("/icons/page_copy.png");
    private static final ImageIcon PASTE_ICON = getIcon("/icons/paste_plain.png");
    private static final ImageIcon DELETE_ICON = getIcon("/icons/page_white_delete.png");
    private static final ImageIcon SELECT_ALL_ICON = getIcon("/icons/accept.png");
    private static final ImageIcon CLOSE_WINDOW_ICON = getIcon("/icons/window-close.png");
    private static final ImageIcon DELETE_WINDOW_ICON = getIcon("/icons/user-trash.png");
    private static final ImageIcon STARRED_ICON = getIcon("/icons/starred.png");
    private static final ImageIcon UNSTARRED_ICON = getIcon("/icons/unstarred.png");

    private JTextArea titleArea = new JTextArea();
    private JTextArea valueArea = new JTextArea();
    private JLabel timestampLabel = new JLabel();
    private JButton deleteButton;
    private JButton closeButton;
    private StarButton starButton;
    private TextChangeListener textChangeListener = new TextChangeListener();
    private StickyNoteData data;
    private JPanel topPanel;

    public StickyNote(StickyNoteData data, boolean visible) {
        super(data.getOcNote().getTitle());
        this.data = data;
        data.addChangeListener(this);

        initGui();

        ComponentResizer componentResizer = new ComponentResizer();
        componentResizer.registerComponent(this);

        valueArea.setText(data.getOcNote().getValue());
        updateTimestamp(data.getOcNote().getModified());
        updateTitle(data.getOcNote().getTitle());
        updateStarred(data.getOcNote().isFavorite());
        setVisible(visible);
        connectTextChangeListener();
    }

    @Override
    public void stickyNoteHasChanged(Object source) {
        if (source != this) {
            SwingUtilities.invokeLater(() -> {
                disConnectTextChangeListener();
                updateDimensions();
                updateTitle(data.getOcNote().getTitle());
                updateValue(data.getOcNote().getValue());
                updateStarred(data.getOcNote().isFavorite());
                updateTimestamp(data.getOcNote().getModified());
                connectTextChangeListener();
            });
        }
    }

    @Override
    public void stickyNoteIsDeleted(StickyNoteData deleted, Object source) {
        if (source != this) {
            setVisible(false);
            data.removeChangeListener(this);
            dispose();
        }
    }

    public boolean hasMoved() {
        return data.isMoved();
    }

    public StickyNoteData getData() {
        return data;
    }

    public void guiConfigChanged() {
        updateTimestampLabel();
        updateTopPanelLaF();
        updateValuePanelLaF();
        getContentPane().repaint();
    }

    public void delete() {
        setVisible(false);
        dispose();
        data.removeChangeListener(StickyNote.this);
        data.delete(StickyNote.this);
    }

    private void initGui() {
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setUndecorated(true);

        updateDimensions();

        setContentPane(new Background());
        getContentPane().setLayout(new BorderLayout());

        initTopPanel();
        initValueArea();
        initBottomPanel();

        setIconImage(StickyNoteIcon.getNoteIcon());
    }

    private void initTopPanel() {
        topPanel = new JPanel(new BorderLayout());
        topPanel.setOpaque(true);
        add(topPanel, BorderLayout.NORTH);

        JPanel titlePanel = new JPanel();
        titlePanel.setOpaque(false);
        titlePanel.setLayout((new BoxLayout(titlePanel, BoxLayout.LINE_AXIS)));
        topPanel.add(titlePanel, BorderLayout.CENTER);

        final Component westResizeArea = new Box.Filler(new Dimension(6, 35), new Dimension(6, 35), new Dimension(6, 35));
        titlePanel.add(westResizeArea);

        titleArea.setOpaque(false);
        titleArea.setBorder(BorderFactory.createEmptyBorder(BORDERWIDTH, 0, BORDERWIDTH, BORDERWIDTH));
        titleArea.addKeyListener(new FocusAdapter(titleArea));
        titleArea.setRows(0);
        titlePanel.add(titleArea, BorderLayout.CENTER);

        DragAdapter dragAdapter = new DragAdapter();
        titleArea.addMouseListener(dragAdapter);
        titleArea.addMouseMotionListener(dragAdapter);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout((new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS)));
        buttonPanel.setOpaque(false);
        topPanel.add(buttonPanel, BorderLayout.EAST);

        deleteButton = createWindowButton(DELETE_WINDOW_ICON, "delete", new DeleteAdapter());
        buttonPanel.add(deleteButton);

        buttonPanel.add(Box.createRigidArea(new Dimension(3, 0)));

        closeButton = createWindowButton(CLOSE_WINDOW_ICON, "close", new CloseAdapter());
        buttonPanel.add(closeButton);

        final Component eastResizeArea = new Box.Filler(new Dimension(6, 35), new Dimension(6, 35), new Dimension(6, 35));
        buttonPanel.add(eastResizeArea);

        updateTopPanelLaF();
    }

    private void initValueArea() {
        JScrollPane scrollPane = new TransparentScrollPane(valueArea);
        scrollPane.setOpaque(false);
        scrollPane.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        scrollPane.getViewport().setOpaque(false);
        scrollPane.setViewportBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        add(scrollPane, BorderLayout.CENTER);
        valueArea.setLineWrap(true);
        valueArea.setOpaque(false);
        valueArea.setBorder(BorderFactory.createEmptyBorder(BORDERWIDTH, BORDERWIDTH, BORDERWIDTH, BORDERWIDTH));
        valueArea.addKeyListener(new FocusAdapter(valueArea));
        new EditAdapter(valueArea);
        updateValuePanelLaF();
    }

    private void initBottomPanel() {
        JPanel bottomPanel = new JPanel(new BorderLayout());
        bottomPanel.setOpaque(false);
        add(bottomPanel, BorderLayout.SOUTH);

        JPanel colorChoosersPanel = new JPanel();
        colorChoosersPanel.setOpaque(false);
        colorChoosersPanel.setLayout((new BoxLayout(colorChoosersPanel, BoxLayout.LINE_AXIS)));
        bottomPanel.add(colorChoosersPanel, BorderLayout.WEST);

        final Component westResizeArea = new Box.Filler(new Dimension(6, 35), new Dimension(6, 35), new Dimension(6, 35));
        colorChoosersPanel.add(westResizeArea);

        starButton = new StarButton();
        colorChoosersPanel.add(starButton);

        for (int i = 0; i < OCN_PROPERTIES.getNoteTopColors().length; i++) {
            colorChoosersPanel.add(new ColorChooser(i));
        }

        JPanel timestampPanel = new JPanel();
        timestampPanel.setLayout((new BoxLayout(timestampPanel, BoxLayout.LINE_AXIS)));
        timestampPanel.setOpaque(false);
        bottomPanel.add(timestampPanel, BorderLayout.EAST);

        timestampLabel.setFont(timestampLabel.getFont().deriveFont(Font.BOLD));
        timestampLabel.setOpaque(false);
        timestampLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        timestampLabel.setBorder(BorderFactory.createEmptyBorder(BORDERWIDTH, BORDERWIDTH, BORDERWIDTH, 0));
        timestampPanel.add(timestampLabel);

        final Component eastResizeArea = new Box.Filler(new Dimension(10, 35), new Dimension(10, 35), new Dimension(10, 35));
        timestampPanel.add(eastResizeArea);
    }

    private JButton createWindowButton(ImageIcon icon, String tooltip, MouseListener actionListener) {
        JButton button = new JButton(icon);
        button.setFocusable(false);
        button.setOpaque(false);
        button.setContentAreaFilled(false);
        button.setBorderPainted(false);
        button.setFocusPainted(false);
        button.setToolTipText(MESSAGES.get(tooltip));
        button.addMouseListener(actionListener);
        button.setSize(22, button.getPreferredSize().height);
        button.setPreferredSize(new Dimension(22, button.getPreferredSize().height));
        return button;
    }

    private void connectTextChangeListener() {
        valueArea.getDocument().addDocumentListener(textChangeListener);
        titleArea.getDocument().addDocumentListener(textChangeListener);
    }

    private void disConnectTextChangeListener() {
        valueArea.getDocument().removeDocumentListener(textChangeListener);
        titleArea.getDocument().removeDocumentListener(textChangeListener);
    }

    private void updateTimestampLabel() {
        updateTimestamp(data.getOcNote().getModified());
    }

    private void updateData() {
        OcNote ocNote = data.getOcNote();
        ocNote.setModified(Instant.now().getEpochSecond());
        ocNote.setContent(titleArea.getText() + "\n" + valueArea.getText());
        ocNote.setFavorite(starButton.starred);
        updateTimestamp(ocNote.getModified());
    }

    private void updateTitle(String title) {
        titleArea.setText(title);
        titleArea.setCaretPosition(title.length());
        setTitle(title);
    }

    private void updateValue(String value) {
        valueArea.setText(value);
        valueArea.setCaretPosition(value.length());
    }

    private void updateStarred(boolean value) {
        starButton.setStarred(value);
    }

    private void updateTimestamp(long timestamp) {
        DateFormat format = new SimpleDateFormat(OCN_PROPERTIES.getTimeFormat());
        timestampLabel.setText(format.format(new Date(timestamp * 1000)));
    }

    private void updateDimensions() {
        Dimensions dimensions = data.getDimensions();
        setSize(dimensions.getWidth(), dimensions.getHeight());
        setLocation(dimensions.getX(), dimensions.getY());
    }

    private void updateTopPanelLaF() {
        topPanel.setBackground(new Color(GlueColor.getGlueColor(OCN_PROPERTIES.getNoteTopColors()[data.getColorIndex()])));
        titleArea.setForeground(new Color(OCN_PROPERTIES.getNoteTitleColor()));
        Font titleFont = new Font(OCN_PROPERTIES.getTitleFontFamily(), OCN_PROPERTIES.getTitleFontStyle(),
                OCN_PROPERTIES.getTitleFontSize());
        titleArea.setFont(titleFont);
    }

    private void updateValuePanelLaF() {
        Font contentFont = new Font(OCN_PROPERTIES.getContentFontFamily(), OCN_PROPERTIES.getContentFontStyle(),
                OCN_PROPERTIES.getContentFontSize());
        valueArea.setFont(contentFont);
        valueArea.setForeground(new Color(OCN_PROPERTIES.getNoteContentColor()));
        timestampLabel.setFont(contentFont.deriveFont(Font.BOLD));
        timestampLabel.setForeground(new Color(OCN_PROPERTIES.getNoteContentColor()));
    }

    private static ImageIcon getIcon(String resource) {
        return new ImageIcon(ClientGui.class.getResource(resource));
    }

    /**
     * Listens for changes in title or content.
     */
    private class TextChangeListener implements DocumentListener {

        @Override
        public void changedUpdate(DocumentEvent arg0) {
            updateData();
        }

        @Override
        public void insertUpdate(DocumentEvent arg0) {
            updateData();
        }

        @Override
        public void removeUpdate(DocumentEvent arg0) {
            updateData();
        }
    }

    /**
     * The FocusAdapter takes care that tabs move focus, but with a modifier insert a tab.
     */
    private class FocusAdapter extends KeyAdapter {

        private JTextComponent component;

        public FocusAdapter(JTextComponent component) {
            this.component = component;
        }

        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_TAB) {
                if (e.getModifiers() == 0) {
                    component.transferFocus();
                    e.consume();
                } else {
                    int caretPosition = component.getCaretPosition();
                    String startText = component.getText().substring(0, caretPosition);
                    String endText = component.getText().substring(caretPosition);
                    component.setText(startText + "\t" + endText);
                }
            }
        }
    }

    /**
     * The MouseCursorAdapter provides the correct mousepointer.
     */
    private class MouseCursorAdapter extends MouseAdapter {

        @Override
        public void mouseExited(MouseEvent e) {
            Cursor cursor = new Cursor(Cursor.DEFAULT_CURSOR);
            setCursor(cursor);
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            Cursor cursor = new Cursor(Cursor.HAND_CURSOR);
            setCursor(cursor);
        }
    }

    /**
     * The DeleteAdapter provides the mouse functionality for the deleteButtonButton.
     */
    private class DeleteAdapter extends MouseCursorAdapter {

        @Override
        public void mousePressed(MouseEvent e) {
            delete();
        }
    }

    private class CloseAdapter extends MouseCursorAdapter {

        @Override
        public void mousePressed(MouseEvent e) {
            setVisible(false);
            dispose();
        }
    }

    /**
     * The DragAdapter provides the mouse functionality for dragging the StickyNote around.
     */
    private class DragAdapter extends MouseInputAdapter {

        private int dragInitialX;
        private int dragInitialY;

        @Override
        public void mousePressed(MouseEvent e) {
            dragInitialX = e.getXOnScreen() - getX();
            dragInitialY = e.getYOnScreen() - getY();
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            int x = e.getXOnScreen() - dragInitialX;
            int y = e.getYOnScreen() - dragInitialY;
            setBounds(x, y, getWidth(), getHeight());
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            Dimensions newDimensions = new Dimensions(getX(), getY(), getWidth(), getHeight());
            data.setDimensions(newDimensions, StickyNote.this);
            data.setMoved(true);
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            Cursor cursor = new Cursor(Cursor.MOVE_CURSOR);
            setCursor(cursor);
        }

        @Override
        public void mouseExited(MouseEvent e) {
            Cursor cursor = new Cursor(Cursor.DEFAULT_CURSOR);
            setCursor(cursor);
        }
    }

    /**
     * The Background class is a specialized JPanel that provides a background with a gradient colour.
     */
    private class Background extends JPanel {

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            Color topColor = new Color(OCN_PROPERTIES.getNoteTopColors()[data.getColorIndex()]);
            Color bottomColor = new Color(OCN_PROPERTIES.getNoteBottomColors()[data.getColorIndex()]);
            Graphics2D g2 = (Graphics2D) g.create();
            GradientPaint gradient = new GradientPaint(0, 0, topColor, getWidth(), this.getHeight(), bottomColor);
            g2.setPaint(gradient);
            g2.fillRect(0, 0, getWidth(), getHeight());
        }
    }

    /**
     * The EditAdapter provides the mouse functionality for edit menu.
     */
    private class EditAdapter extends MouseAdapter implements FocusListener {

        private JTextArea parent;
        private JPopupMenu menu;

        public EditAdapter(JTextArea parent) {
            this.parent = parent;
            parent.addMouseListener(this);
            parent.addFocusListener(this);
            menu = createEditMenu();
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            parent.requestFocus();

            // Left button
            if (e.getButton() == MouseEvent.BUTTON3) {
                menu.setLocation(e.getXOnScreen() + 1, e.getYOnScreen() + 1);
                menu.setVisible(true);
            } else {
                menu.setVisible(false);
            }
        }

        private JPopupMenu createEditMenu() {
            final JPopupMenu menu = new JPopupMenu(MESSAGES.get("edit"));

            final JMenuItem cutItem = new JMenuItem(MESSAGES.get("cut"), CUT_ICON);
            cutItem.addActionListener(e -> {
                StringSelection selection = new StringSelection(parent.getSelectedText());
                Toolkit.getDefaultToolkit().getSystemClipboard().setContents(selection, null);
                parent.replaceRange("", parent.getSelectionStart(), parent.getSelectionEnd());
                menu.setVisible(false);
            });
            menu.add(cutItem);

            JMenuItem copyItem = new JMenuItem(MESSAGES.get("copy"), COPY_ICON);
            copyItem.addActionListener(e -> {
                StringSelection selection = new StringSelection(parent.getSelectedText());
                Toolkit.getDefaultToolkit().getSystemClipboard().setContents(selection, null);
                menu.setVisible(false);
            });
            menu.add(copyItem);

            JMenuItem pasteItem = new JMenuItem(MESSAGES.get("paste"), PASTE_ICON);
            pasteItem.addActionListener(e -> {
                Transferable clipboardContents = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);

                if (clipboardContents != null && clipboardContents.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                    try {
                        String pasted = (String) clipboardContents.getTransferData(DataFlavor.stringFlavor);
                        parent.replaceRange(pasted, parent.getSelectionStart(), parent.getSelectionEnd());
                        menu.setVisible(false);
                    } catch (UnsupportedFlavorException | IOException ex) {
                        LOG.log(LogLevel.ERROR, "Unable to paste content", ex);
                    }
                }
            });
            menu.add(pasteItem);

            JMenuItem deleteItem = new JMenuItem(MESSAGES.get("delete"), DELETE_ICON);
            deleteItem.addActionListener(e -> {
                parent.replaceRange("", parent.getSelectionStart(), parent.getSelectionEnd());
                menu.setVisible(false);
            });
            menu.add(deleteItem);

            menu.addSeparator();

            JMenuItem selectAllItem = new JMenuItem(MESSAGES.get("select_all"), SELECT_ALL_ICON);
            selectAllItem.addActionListener(e -> {
                parent.setSelectionStart(0);
                parent.setSelectionEnd(parent.getText().length());
                menu.setVisible(false);
            });
            menu.add(selectAllItem);

            return menu;
        }

        @Override
        public void focusGained(FocusEvent e) {
            // nothing
        }

        @Override
        public void focusLost(FocusEvent e) {
            menu.setVisible(false);
        }
    }

    private class StarButton extends JButton {

        private boolean starred;

        public StarButton() {
            setFocusable(false);
            setOpaque(false);
            setContentAreaFilled(false);
            setBorderPainted(false);
            setFocusPainted(false);
            setToolTipText(MESSAGES.get("favorite"));
            setSize(22, getPreferredSize().height);
            setPreferredSize(new Dimension(22, getPreferredSize().height));
            addActionListener(a -> setStarred(!starred));
            addMouseListener(new MouseCursorAdapter());
        }

        public void setStarred(boolean starred) {
            this.starred = starred;
            setIcon(starred ? STARRED_ICON : UNSTARRED_ICON);
            updateData();
        }
    }

    private class ColorChooser extends JPanel implements MouseListener {

        private int index;

        public ColorChooser(int index) {
            this.index = index;
            setOpaque(false);
            addMouseListener(new MouseAdapter() {

                @Override
                public void mouseClicked(MouseEvent e) {
                    data.setColorIndex(index);
                    StickyNote.this.repaint();
                    updateTopPanelLaF();
                    updateValuePanelLaF();
                }
            });
            addMouseListener(new MouseCursorAdapter());
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            Color topColor = new Color(OCN_PROPERTIES.getNoteTopColors()[index]);
            Color bottomColor = new Color(OCN_PROPERTIES.getNoteBottomColors()[index]);
            Graphics2D g2 = (Graphics2D) g.create();
            GradientPaint gradient = new GradientPaint(0, 0, topColor, getWidth(), this.getHeight(), bottomColor);
            g2.setPaint(gradient);
            g2.fillRect(0, 10, getWidth(), getHeight() - 10);
        }

        @Override
        public void mouseExited(MouseEvent e) {
            Cursor cursor = new Cursor(Cursor.DEFAULT_CURSOR);
            setCursor(cursor);
        }

        @Override
        public void mouseClicked(final MouseEvent e) {
        }

        @Override
        public void mousePressed(final MouseEvent e) {
        }

        @Override
        public void mouseReleased(final MouseEvent e) {
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            Cursor cursor = new Cursor(Cursor.HAND_CURSOR);
            setCursor(cursor);
        }
    }

    public static void main(String[] args) {
        AntiAliaser.antiAliasing();
        LookAndFeel.loadLaf(new String[]{}, new LookAndFeel.Properties() {
            @Override
            public String getLaf() {
                return "GTK+";
            }

            @Override
            public void setLaf(final String laf) {
            }
        });
        OcNote ocNote = new OcNote(1, System.currentTimeMillis() / 1000, "Test note\nShow content");
        StickyNoteData stickyNoteData = new StickyNoteData(ocNote, new Dimensions(400, 400, 600, 400));
        new StickyNote(stickyNoteData, true);
    }
}
