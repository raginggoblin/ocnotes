/*
 * Copyright 2015, RagingGoblin <http://raginggoblin.wordpress.com>
 *
 * This file is part of OcNotes.
 *
 *  OcNotess is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OcNotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OcNotes.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.ocnotes.client.gui;

import java.awt.*;
import java.awt.image.BufferedImage;

import raging.goblin.ocnotes.client.properties.OcNotesProperties;

/**
 * The StickyNoteIcon creates BufferedImages to use as icons for the StickyNotes on the task bar of your OS.
 */
public abstract class StickyNoteIcon {

    private static final OcNotesProperties PROPERTIES = OcNotesProperties.getInstance();

    private static BufferedImage noteIcon;

    public static BufferedImage getNoteIcon() {
        if (noteIcon == null) {
            noteIcon = createIcon(PROPERTIES.getNoteTopColors()[0], "OcNote");
        }
        return noteIcon;
    }

    public static void colorsChanged() {
        noteIcon = null;
        getNoteIcon();
    }

    private static BufferedImage createIcon(int background, String title) {
        BufferedImage image = new BufferedImage(100, 100, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = image.createGraphics();

        g2.setColor(new Color(background));
        g2.fillRect(10, 10, 80, 80);

        g2.setColor(new Color(GlueColor.getGlueColor(background)));
        g2.fillRect(10, 10, 80, 10);

        g2.setColor(Color.BLACK);
        g2.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 5));
        g2.drawString(title, 15, 19);

        return image;
    }
}
