/*
 * Copyright 2015, RagingGoblin <http://raginggoblin.wordpress.com>
 * 
 * This file is part of OcNotes.
 *
 *  OcNotess is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OcNotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OcNotes.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.ocnotes.client.gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.JSpinner.DefaultEditor;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import raging.goblin.ocnotes.client.Messages;
import raging.goblin.ocnotes.client.SynchronizationHandler;
import raging.goblin.ocnotes.client.properties.OcNotesProperties;
import raging.goblin.swingutils.ScreenPositioner;

public class SyncConfigDialog extends JDialog {

	private static final OcNotesProperties PROPERTIES = OcNotesProperties.getInstance();
	private static final Messages MESSAGES = Messages.getInstance();

	private static final int DISABLED_INTERVAL = -1;
	private static final int ENABLED_INTERVAL = 10;

	private static final String TITLE = MESSAGES.get("sync_title");
	private static final String ENABLE_TOOLTIP = MESSAGES.get("enable_tooltip");
	private static final String HOST_TOOLTIP = MESSAGES.get("host_tooltip");
	private static final String FOLDER_TOOLTIP = MESSAGES.get("folder_tooltip");
	private static final String USER_TOOLTIP = MESSAGES.get("user_tooltip");
	private static final String PASSWORD_TOOLTIP = MESSAGES.get("password_tooltip");
	private static final String INTERVAL_TOOLTIP = MESSAGES.get("interval_tooltip");
	private static final String LOGIN_SUCCESS_TITLE = MESSAGES.get("login_success_title");
	private static final String LOGIN_SUCCESS = MESSAGES.get("login_success");
	private static final String LOGIN_FAILED_TITLE = MESSAGES.get("login_failed_title");
	private static final String LOGIN_FAILED = MESSAGES.get("login_failed");

	private JTextField hostField;
	private JTextField userField;
	private JPasswordField passwordField;
	private JSpinner intervalSpinner;
	private JSpinner portSpinner;
	private JCheckBox enableBox;
	private SynchronizationHandler synchronizationHandler;
	private JCheckBox httpsCBox;
	private JTextField folderField;

	public SyncConfigDialog(JFrame parent, final SynchronizationHandler synchronizationHandler) {
		super(parent, TITLE, true);
		this.synchronizationHandler = synchronizationHandler;
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setSize(new Dimension(400, 400));
		ScreenPositioner.centerOnScreen(this);

		JPanel configPanel = new JPanel();
		getContentPane().add(configPanel, BorderLayout.NORTH);
		configPanel.setLayout(new FormLayout(new ColumnSpec[] { ColumnSpec.decode("max(5dlu;default)"),
				ColumnSpec.decode("right:default"), ColumnSpec.decode("max(5dlu;default)"),
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("76px:grow"), ColumnSpec.decode("default:grow"),
				FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("max(5dlu;default)"), },
				new RowSpec[] { FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("28px"),
						FormFactory.NARROW_LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
						RowSpec.decode("28px"), FormFactory.NARROW_LINE_GAP_ROWSPEC, RowSpec.decode("28px"),
						FormFactory.NARROW_LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
						RowSpec.decode("28px"), FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
						FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
						FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, RowSpec.decode("30px"),
						FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

		JLabel lblEnableS = new JLabel(MESSAGES.get("enable_synchronization"));
		configPanel.add(lblEnableS, "2, 1");

		enableBox = new JCheckBox("", PROPERTIES.isSynchronizationEnabled());
		enableBox.setToolTipText(ENABLE_TOOLTIP);
		configPanel.add(enableBox, "5, 1");

		JLabel lblServerHost = new JLabel(MESSAGES.get("server_host"));
		configPanel.add(lblServerHost, "2, 3, right, center");

		hostField = new JTextField();
		hostField.setToolTipText(HOST_TOOLTIP);
		configPanel.add(hostField, "5, 3, 2, 1, fill, center");
		hostField.setColumns(10);

		JLabel lblFolder = new JLabel(MESSAGES.get("folder"));
		configPanel.add(lblFolder, "2, 5");

		folderField = new JTextField();
		folderField.setToolTipText(FOLDER_TOOLTIP);
		configPanel.add(folderField, "5, 5, 2, 1, fill, default");
		folderField.setColumns(10);

		JLabel lblServerPort = new JLabel(MESSAGES.get("port_number"));
		configPanel.add(lblServerPort, "2, 7, right, center");

		portSpinner = new JSpinner();
		portSpinner.setModel(new SpinnerNumberModel(new Integer(OcNotesProperties.DEFAULT_SERVERPORT), null, null,
				new Integer(1)));
		portSpinner.setEditor(new JSpinner.NumberEditor(portSpinner, "#"));
		configPanel.add(portSpinner, "5, 7, fill, default");

		httpsCBox = new JCheckBox("Https");
		httpsCBox.addActionListener(e -> {
			if (httpsCBox.isSelected()) {
				portSpinner.setValue(OcNotesProperties.DEFAULT_HTTPS_SERVERPORT);
			} else {
				portSpinner.setValue(OcNotesProperties.DEFAULT_SERVERPORT);
			}
		});
		httpsCBox.setHorizontalTextPosition(SwingConstants.LEFT);
		configPanel.add(httpsCBox, "6, 7, right, center");

		JLabel lblUsername = new JLabel(MESSAGES.get("username"));
		configPanel.add(lblUsername, "2, 9, right, center");

		userField = new JTextField();
		userField.setToolTipText(USER_TOOLTIP);
		configPanel.add(userField, "5, 9, 2, 1, fill, center");
		userField.setColumns(10);

		JLabel lblPassword = new JLabel(MESSAGES.get("password"));
		configPanel.add(lblPassword, "2, 11, right, center");

		passwordField = new JPasswordField();
		passwordField.setToolTipText(PASSWORD_TOOLTIP);
		configPanel.add(passwordField, "5, 11, 2, 1, fill, center");

		JLabel lblSynchronizationInterval = new JLabel(MESSAGES.get("interval"));
		configPanel.add(lblSynchronizationInterval, "2, 13, right, center");

		intervalSpinner = new JSpinner();
		intervalSpinner.setToolTipText(INTERVAL_TOOLTIP);
		intervalSpinner.setModel(new SpinnerNumberModel(new Integer(10), new Integer(1), null, new Integer(1)));
		((DefaultEditor) intervalSpinner.getEditor()).getTextField().setColumns(4);
		configPanel.add(intervalSpinner, "5, 13, fill, center");

		JLabel lblMin = new JLabel("min");
		configPanel.add(lblMin, "6, 13, left, center");

		JButton btnTestCredentials = new JButton(MESSAGES.get("test_credentials"));
		btnTestCredentials.addActionListener(e -> tryLogin());

		configPanel.add(btnTestCredentials, "5, 18, 2, 1, fill, center");

		JSeparator separator = new JSeparator();
		configPanel.add(separator, "2, 21, 5, 1");

		JPanel actionPanel = new JPanel();
		getContentPane().add(actionPanel, BorderLayout.SOUTH);
		actionPanel.setLayout(new FormLayout(new ColumnSpec[] { ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("max(40dlu;default)"), FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC, FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("max(40dlu;default)"),
				FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("max(5dlu;default)"), }, new RowSpec[] {
				FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, RowSpec.decode("max(5dlu;default)"), }));

		JButton btnCancel = new JButton(MESSAGES.get("cancel"));
		btnCancel.addActionListener(e -> setVisible(false));
		actionPanel.add(btnCancel, "3, 1, 2, 1");

		JButton btnOk = new JButton(MESSAGES.get("ok"));
		btnOk.addActionListener(e -> {
			setVisible(false);
			saveConfiguration();
		});
		actionPanel.add(btnOk, "7, 1, 2, 1");

		loadConfiguration();
		setFocus(btnCancel);
	}

	private void loadConfiguration() {
		hostField.setText(PROPERTIES.getServerHostName());
		folderField.setText(PROPERTIES.getFolder());
		userField.setText(PROPERTIES.getUsername());
		passwordField.setText(PROPERTIES.getPassword());
		intervalSpinner.setValue(PROPERTIES.isSynchronizationEnabled() ? PROPERTIES.getPollInterval() : ENABLED_INTERVAL);
		portSpinner.setValue(PROPERTIES.getServerPort());
		httpsCBox.setSelected(PROPERTIES.isHttps());
	}

	private void setFocus(final JComponent component) {
		SwingUtilities.invokeLater(() -> component.requestFocusInWindow());
	}

	private void tryLogin() {
		boolean success = synchronizationHandler.tryRemoteLogin(hostField.getText(), folderField.getText(),
				(Integer) portSpinner.getValue(), userField.getText(), new String(passwordField.getPassword()),
				httpsCBox.isSelected());
		if (success) {
			JOptionPane.showMessageDialog(this, LOGIN_SUCCESS, LOGIN_SUCCESS_TITLE, JOptionPane.INFORMATION_MESSAGE);
		} else {
			JOptionPane.showMessageDialog(this, LOGIN_FAILED, LOGIN_FAILED_TITLE, JOptionPane.ERROR_MESSAGE);
		}
	}

	private void saveConfiguration() {
		PROPERTIES.setServerHostName(hostField.getText());
		PROPERTIES.setFolder(folderField.getText());
		PROPERTIES.setServerPort((Integer) portSpinner.getValue());
		PROPERTIES.setHttps(httpsCBox.isSelected());
		PROPERTIES.setUsername(userField.getText());
		PROPERTIES.setPassword(new String(passwordField.getPassword()));
		if (enableBox.isSelected()) {
			PROPERTIES.setPollInterval((Integer) intervalSpinner.getValue());
		} else {
			PROPERTIES.setPollInterval(DISABLED_INTERVAL);
		}
	}
}
