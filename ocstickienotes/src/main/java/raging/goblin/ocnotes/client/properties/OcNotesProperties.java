/*
 * Copyright 2015, RagingGoblin <http://raginggoblin.wordpress.com>
 *
 * This file is part of OcNotes.
 *
 *  OcNotes is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  OcNotes is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with OcNotes.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.ocnotes.client.properties;

import java.util.Arrays;
import java.util.Locale;
import java.util.prefs.Preferences;

import org.jasypt.exceptions.EncryptionOperationNotPossibleException;
import org.jasypt.util.text.BasicTextEncryptor;

import raging.goblin.ocnotes.client.LogForJLogger;
import raging.goblin.ocnotes.sync.log.LogLevel;
import raging.goblin.ocnotes.sync.log.Logger;

public class OcNotesProperties {

    private static final String ENCRYPTION_PASSWORD = "RagingGoblinRagesAtTheWorld";
    private static final Logger LOG = new LogForJLogger(OcNotesProperties.class);

    // Synchronization
    private static final String USERNAME_KEY = "username";
    private static final String DEFAULT_USERNAME = "";
    private static final String PASSWORD_KEY = "password";
    private static final String DEFAULT_PASSWORD = "";
    private static final String HOSTNAME_KEY = "hostname";
    private static final String DEFAULT_HOSTNAME = "localhost";
    private static final String FOLDER_KEY = "folder";
    private static final String DEFAULT_FOLDER = "owncloud";
    private static final String HTTPS_KEY = "https";
    private static final boolean DEFAULT_HTTPS = false;
    private static final String KEYSTORE_KEY = "keystore";
    private static final String DEFAULT_KEYSTORE = "ocnotescerts";
    private static final String PASSPHRASE_KEY = "passphrase";
    private static final String DEFAULT_PASSPHRASE = "changeit";
    private static final String SERVERPORT_KEY = "serverport";
    public static final int DEFAULT_SERVERPORT = 80;
    public static final int DEFAULT_HTTPS_SERVERPORT = 443;
    private static final String POLLINTERVAL_KEY = "pollinterval";
    public static final int DEFAULT_POLLINTERVAL = -1;

    // Look and feel
    public static final String DEFAULT_TRAY_ICON = "notes.png";
    public static final int DEFAULT_DOUBLE_CLICK_DELAY = 250;
    public static final int DEFAULT_DRAG_DELAY = 2;
    public static final int DEFAULT_SPACING = 40;
    public static final int HORIZONTAL_SPACING = 25;
    public static final int SCREEN_BORDER_SPACING = 100;
    public static final int DEFAULT_HEIGHT = 250;
    public static final int DEFAULT_WIDTH = 200;
    public static final String DEFAULT_NOTE_TOP_COLORS = "-65904,-94793,-10314299,-9110156,-2853634,-147893";
    public static final String DEFAULT_NOTE_BOTTOM_COLORS = "-65851,-19247,-5387553,-5046862,-1658370,-10873";
    public static final int DEFAULT_NOTE_TITLE_COLOR = -16777216;
    public static final int DEFAULT_NOTE_CONTENT_COLOR = -16777216;
    public static final String DEFAULT_TIME_FORMAT = "dd-MM-yyyy HH:mm";
    public static final long DEFAULT_LAST_SYNCTIME = 0;
    public static final String DEFAULT_TITLE_FONT_FAMILY = "SansSerif";
    public static final int DEFAULT_TITLE_FONT_STYLE = 1;
    public static final int DEFAULT_TITLE_FONT_SIZE = 12;
    public static final String DEFAULT_CONTENT_FONT_FAMILY = "SansSerif";
    public static final int DEFAULT_CONTENT_FONT_STYLE = 0;
    public static final int DEFAULT_CONTENT_FONT_SIZE = 12;

    private static final String TRAY_ICON_KEY = "trayicon";
    private static final String DOUBLE_CLICK_DELAY_KEY = "dubbleclickdelay";
    private static final String DRAG_DELAY_KEY = "dragdelay";
    private static final String SPACING_KEY = "spacing";
    private static final String HEIGHT_KEY = "height";
    private static final String WIDTH_KEY = "widht";
    private static final String NOTE_TOP_COLORS_KEY = "topcolors";
    private static final String NOTE_TITLE_COLOR_KEY = "titlecolor";
    private static final String NOTE_CONTENT_COLOR_KEY = "contentcolor";
    private static final String NOTE_BOTTOM_COLORS_KEY = "bottomcolors";
    private static final String TIME_FORMAT_KEY = "timeformat";
    private static final String LAST_SYNCTIME_KEY = "lastsynctime";
    private static final String LANGUAGE_KEY = "language";
    private static final String FIRST_RUN_KEY = "firstrun";
    private static final String TITLE_FONT_FAMILY_KEY = "titlefontname";
    private static final String TITLE_FONT_STYLE_KEY = "titlefontstyle";
    private static final String TITLE_FONT_SIZE_KEY = "titlefontsize";
    private static final String CONTENT_FONT_FAMILY_KEY = "contentfontname";
    private static final String CONTENT_FONT_STYLE_KEY = "contentfontstyle";
    private static final String CONTENT_FONT_SIZE_KEY = "contentfontsize";

    private static OcNotesProperties instance;
    private static Preferences userPreferences = Preferences.userNodeForPackage(OcNotesProperties.class);

    private OcNotesProperties() {
        // Singleton
    }

    public static OcNotesProperties getInstance() {
        if (instance == null) {
            instance = new OcNotesProperties();
        }
        return instance;
    }

    public String getUsername() {
        return userPreferences.get(USERNAME_KEY, DEFAULT_USERNAME);
    }

    public void setUsername(String username) {
        userPreferences.put(USERNAME_KEY, username);
    }

    public String getPassword() {
        String encryptedPassword = userPreferences.get(PASSWORD_KEY, DEFAULT_PASSWORD);
        if (encryptedPassword.equals(DEFAULT_PASSWORD)) {
            return DEFAULT_PASSWORD;
        }

        BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
        textEncryptor.setPassword(ENCRYPTION_PASSWORD);
        try {
            return textEncryptor.decrypt(encryptedPassword);
        } catch (EncryptionOperationNotPossibleException e) {
            LOG.log(LogLevel.ERROR, "Unable to decrypt password", e);
        }

        return DEFAULT_PASSWORD;
    }

    public void setPassword(String password) {
        BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
        textEncryptor.setPassword(ENCRYPTION_PASSWORD);
        String encryptedPassword = textEncryptor.encrypt(password);
        userPreferences.put(PASSWORD_KEY, encryptedPassword);
    }

    public String getServerHostName() {
        return userPreferences.get(HOSTNAME_KEY, DEFAULT_HOSTNAME);
    }

    public void setServerHostName(String serverHostName) {
        userPreferences.put(HOSTNAME_KEY, serverHostName);
    }

    public String getFolder() {
        return userPreferences.get(FOLDER_KEY, DEFAULT_FOLDER);
    }

    public void setFolder(String folder) {
        userPreferences.put(FOLDER_KEY, folder);
    }

    public boolean isHttps() {
        return userPreferences.getBoolean(HTTPS_KEY, DEFAULT_HTTPS);
    }

    public void setHttps(boolean https) {
        userPreferences.putBoolean(HTTPS_KEY, https);
    }

    public String getKeystore() {
        return userPreferences.get(KEYSTORE_KEY, DEFAULT_KEYSTORE);
    }

    public void setKeystore(String keystore) {
        userPreferences.put(KEYSTORE_KEY, keystore);
    }

    public String getPassphrase() {
        return userPreferences.get(PASSPHRASE_KEY, DEFAULT_PASSPHRASE);
    }

    public void setPassphrase(String passphrase) {
        userPreferences.put(PASSWORD_KEY, passphrase);
    }

    public int getServerPort() {
        return userPreferences.getInt(SERVERPORT_KEY, DEFAULT_SERVERPORT);
    }

    public void setServerPort(int serverPort) {
        userPreferences.putInt(SERVERPORT_KEY, serverPort);
    }

    public int getPollInterval() {
        return userPreferences.getInt(POLLINTERVAL_KEY, DEFAULT_POLLINTERVAL);
    }

    public void setPollInterval(int pollInterval) {
        userPreferences.putInt(POLLINTERVAL_KEY, pollInterval);
    }

    public boolean isSynchronizationEnabled() {
        return getPollInterval() > 0;
    }

    public String getTrayIcon() {
        return userPreferences.get(TRAY_ICON_KEY, DEFAULT_TRAY_ICON);
    }

    public void setTrayIcon(String trayIcon) {
        userPreferences.put(TRAY_ICON_KEY, trayIcon);
    }

    public int getDoubleClickDelay() {
        return userPreferences.getInt(DOUBLE_CLICK_DELAY_KEY, DEFAULT_DOUBLE_CLICK_DELAY);
    }

    public void setDoubleClickDelay(int doubleClickDelay) {
        userPreferences.putInt(DOUBLE_CLICK_DELAY_KEY, doubleClickDelay);
    }

    public int getDragDelay() {
        return userPreferences.getInt(DRAG_DELAY_KEY, DEFAULT_DRAG_DELAY);
    }

    public void setDragDelay(int dragDelay) {
        userPreferences.putInt(DRAG_DELAY_KEY, dragDelay);
    }

    public int getSpacing() {
        return userPreferences.getInt(SPACING_KEY, DEFAULT_SPACING);
    }

    public void setSpacing(int spacing) {
        userPreferences.putInt(SPACING_KEY, spacing);
    }

    public int getHeight() {
        return userPreferences.getInt(HEIGHT_KEY, DEFAULT_HEIGHT);
    }

    public void setHeight(int height) {
        userPreferences.putInt(HEIGHT_KEY, height);
    }

    public int getWidth() {
        return userPreferences.getInt(WIDTH_KEY, DEFAULT_WIDTH);
    }

    public void setWidth(int width) {
        userPreferences.putInt(WIDTH_KEY, width);
    }

    public int[] getNoteTopColors() {
        return fromCommaSeparatedString(userPreferences.get(NOTE_TOP_COLORS_KEY, DEFAULT_NOTE_TOP_COLORS));
    }

    public void setNoteTopColors(String noteTopColors) {
        userPreferences.put(NOTE_TOP_COLORS_KEY, noteTopColors);
    }

    public int[] getNoteBottomColors() {
        return fromCommaSeparatedString(userPreferences.get(NOTE_BOTTOM_COLORS_KEY, DEFAULT_NOTE_BOTTOM_COLORS));
    }

    public void setNoteBottomColors(String noteBottomColors) {
        userPreferences.put(NOTE_BOTTOM_COLORS_KEY, noteBottomColors);
    }

    public int getNoteTitleColor() {
        return userPreferences.getInt(NOTE_TITLE_COLOR_KEY, DEFAULT_NOTE_TITLE_COLOR);
    }

    public void setNoteTitleColor(int noteTitleColor) {
        userPreferences.putInt(NOTE_TITLE_COLOR_KEY, noteTitleColor);
    }

    public int getNoteContentColor() {
        return userPreferences.getInt(NOTE_CONTENT_COLOR_KEY, DEFAULT_NOTE_CONTENT_COLOR);
    }

    public void setNoteContentColor(int noteContentColor) {
        userPreferences.putInt(NOTE_CONTENT_COLOR_KEY, noteContentColor);
    }

    public String getTimeFormat() {
        return userPreferences.get(TIME_FORMAT_KEY, DEFAULT_TIME_FORMAT);
    }

    public void setTimeFormat(String timeFormat) {
        userPreferences.put(TIME_FORMAT_KEY, timeFormat);
    }

    public long getLastSyncTime() {
        return userPreferences.getLong(LAST_SYNCTIME_KEY, DEFAULT_LAST_SYNCTIME);
    }

    public void setLastSyncTime(long lastSyncTime) {
        userPreferences.putLong(LAST_SYNCTIME_KEY, lastSyncTime);
    }

    public String getLanguage() {
        Locale currentLocale = Locale.getDefault();
        return userPreferences.get(LANGUAGE_KEY, currentLocale.getLanguage());
    }

    public void setLanguage(String language) {
        userPreferences.put(LANGUAGE_KEY, language);
    }

    public boolean isFirstRun() {
        return userPreferences.getBoolean(FIRST_RUN_KEY, true);
    }

    public void setFirstRun(boolean b) {
        userPreferences.putBoolean(FIRST_RUN_KEY, false);
    }

    public void setTitleFontFamily(String titleFontFamily) {
        userPreferences.put(TITLE_FONT_FAMILY_KEY, titleFontFamily);
    }

    public String getTitleFontFamily() {
        return userPreferences.get(TITLE_FONT_FAMILY_KEY, DEFAULT_TITLE_FONT_FAMILY);
    }

    public void setTitleFontStyle(int titleFontStyle) {
        userPreferences.putInt(TITLE_FONT_STYLE_KEY, titleFontStyle);
    }

    public int getTitleFontStyle() {
        return userPreferences.getInt(TITLE_FONT_STYLE_KEY, DEFAULT_TITLE_FONT_STYLE);
    }

    public void setTitleFontSize(int titleFontSize) {
        userPreferences.putInt(TITLE_FONT_SIZE_KEY, titleFontSize);
    }

    public int getTitleFontSize() {
        return userPreferences.getInt(TITLE_FONT_SIZE_KEY, DEFAULT_TITLE_FONT_SIZE);
    }

    public void setContentFontFamily(String contentFontFamily) {
        userPreferences.put(CONTENT_FONT_FAMILY_KEY, contentFontFamily);
    }

    public String getContentFontFamily() {
        return userPreferences.get(CONTENT_FONT_FAMILY_KEY, DEFAULT_CONTENT_FONT_FAMILY);
    }

    public void setContentFontStyle(int contentFontStyle) {
        userPreferences.putInt(CONTENT_FONT_STYLE_KEY, contentFontStyle);
    }

    public int getContentFontStyle() {
        return userPreferences.getInt(CONTENT_FONT_STYLE_KEY, DEFAULT_CONTENT_FONT_STYLE);
    }

    public void setContentFontSize(int contentFontSize) {
        userPreferences.putInt(CONTENT_FONT_SIZE_KEY, contentFontSize);
    }

    public int getContentFontSize() {
        return userPreferences.getInt(CONTENT_FONT_SIZE_KEY, DEFAULT_CONTENT_FONT_SIZE);
    }

    public int[] getDefaultNoteBottomColors() {
        return fromCommaSeparatedString(DEFAULT_NOTE_BOTTOM_COLORS);
    }

    public int[] getDefaultNoteTopColors() {
        return fromCommaSeparatedString(DEFAULT_NOTE_TOP_COLORS);
    }

    private int[] fromCommaSeparatedString(String value) {
        return Arrays.stream(value.split(",")).mapToInt(v -> Integer.parseInt(v)).toArray();
    }
}
